#!/bin/bash -e
# Debian ISO downloader
# Version 1.0
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022-2024 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

source ./common.shlib

url_debian_release=https://cdimage.debian.org/cdimage/release
url_debian_archive=https://cdimage.debian.org/cdimage/archive
url_debian_testing=https://cdimage.debian.org/cdimage
url_debian_fw_testing=https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware
url_debian_fw_release=https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware
url_debian_fw_archive=https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/archive
url_yandex_release=https://mirror.yandex.ru/debian-cd

echo "" >./debian.tmp
cat\
 debian-7.11.0.head.sh\
 debian-8.11.1.head.sh\
 debian-9.13.0.head.sh\
 debian-10.13.0.head.sh\
 debian-11.11.0.head.sh\
 debian-12.8.0.head.sh\
 debian-testing.head.sh\
 >>./debian.tmp
source ./debian.tmp
rm -f ./debian.tmp

select_category DLTYPE 'Debian GNU/Linux downloader' 'Choose a version to download:'

OUTPUT=$DLTYPE

cat >./debian.tmp <<EOT
case "$DLTYPE" in
EOT
cat\
 debian-7.11.0.body.sh\
 debian-8.11.1.body.sh\
 debian-9.13.0.body.sh\
 debian-10.13.0.body.sh\
 debian-11.11.0.body.sh\
 debian-12.8.0.body.sh\
 debian-testing.body.sh\
 >>./debian.tmp
cat >>./debian.tmp <<EOT
*)
	echo 'Unknown option selected.' >&2
	exit 1
	;;
esac
EOT
source ./debian.tmp
rm -f ./debian.tmp

select_and_download_files "$DLTYPE" "$URL" "$OUTPUT"

cd "$OUTPUT"
check_sums
cd "$OLDPWD"

echo 'Done.' >&2
