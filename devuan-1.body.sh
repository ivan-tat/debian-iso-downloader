# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

devuan-1.0.0)
	DLTYPE="Devuan GNU+Linux 1.0.0 / 2017-05-04 (Jessie) Information"
	URL=$url_devuan_1_0_0
	add_dialog_item README.txt 'README.txt | 1453' 1
	;;
devuan-1.0.0-desktop-live)
	DLTYPE="Devuan GNU+Linux 1.0.0 / 2017-05-04 (Jessie) Desktop Live DVDs"
	URL=$url_devuan_1_0_0/desktop-live
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                           'README.txt                   | 6105' 1
	add_dialog_item devuan_jessie_1.0.0_amd64_desktop-live.iso           'AMD64 Desktop Live DVD       | 828M' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_desktop-live.list.txt      ' * .list.txt file            |  39K' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_uefi_desktop-live.iso      'AMD64 Desktop Live DVD (UEFI)| 836M' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_uefi_desktop-live.list.txt ' * .list.txt file            |  39K' 0
	add_dialog_item devuan_jessie_1.0.0_i386_desktop-live.iso            'i386 Desktop Live DVD        | 848M' 0
	add_dialog_item devuan_jessie_1.0.0_i386_desktop-live.list.txt       ' * .list.txt file            |  39K' 0
	;;
devuan-1.0.0-minimal-live)
	DLTYPE="Devuan GNU+Linux 1.0.0 / 2017-05-04 (Jessie) Minimal Live CDs"
	URL=$url_devuan_1_0_0/minimal-live
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                      'README.txt            | 6509' 1
	add_dialog_item devuan_jessie_1.0.0_amd64_minimal-live.iso      'AMD64 Minimal Live CD | 305M' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_minimal-live_list.txt ' * _list.txt file     |  15K' 0
	add_dialog_item devuan_jessie_1.0.0_i386_minimal-live.iso       'i386 Minimal Live CD  | 299M' 0
	add_dialog_item devuan_jessie_1.0.0_i386_minimal-live_list.txt  ' * _list.txt file     |  14K' 0
	;;
devuan-1.0.0-installer-iso)
	DLTYPE="Devuan GNU+Linux 1.0.0 / 2017-05-04 (Jessie) Installer CDs & DVDs"
	URL=$url_devuan_1_0_0/installer-iso
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                'README.txt                 | 1771' 1
	add_dialog_item devuan_jessie_1.0.0_amd64_CD.iso          'AMD64 Installer CD         | 646M' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_CD.list.gz      ' * .list.gz file           |  13K' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_DVD.iso         'AMD64 Installer DVD        | 4.4G' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_DVD.list.gz     ' * .list.gz file           |  42K' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_NETINST.iso     'AMD64 Network installer CD | 227M' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_NETINST.list.gz ' * .list.gz file           | 5106' 0
	add_dialog_item devuan_jessie_1.0.0_i386_CD.iso           'i386 Installer CD          | 632M' 0
	add_dialog_item devuan_jessie_1.0.0_i386_CD.list.gz       ' * .list.gz file           |  12K' 0
	add_dialog_item devuan_jessie_1.0.0_i386_DVD.iso          'i386 Installer DVD         | 3.1G' 0
	add_dialog_item devuan_jessie_1.0.0_i386_DVD.list.gz      ' * .list.gz file           |  42K' 0
	add_dialog_item devuan_jessie_1.0.0_i386_NETINST.iso      'i386 Network installer CD  | 267M' 0
	add_dialog_item devuan_jessie_1.0.0_i386_NETINST.list.gz  ' * .list.gz file           | 5317' 0
	;;
devuan-1.0.0-virtual)
	DLTYPE="Devuan GNU+Linux 1.0.0 / 2017-05-04 (Jessie) Virtual images"
	URL=$url_devuan_1_0_0/virtual
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                 'README.txt                         |  608' 1
	add_dialog_item devuan_jessie_1.0.0_amd64_virtual.qcow2.xz 'AMD64 QEMU qcow2 image (.qcow2.xz) | 526M' 0
	add_dialog_item devuan_jessie_1.0.0_amd64_vagrant.box      'AMD64 Vagrant image (.box)         | 669M' 0
	add_dialog_item Vagrantfile                                ' * Vagrantfile                     |  290' 0
	;;
