# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_devuan_5=$url_devuan_release/devuan_daedalus

add_dialog_item devuan-5                   '5.x.x / 2023-08-14 (Daedalus) Information' 0
add_dialog_item devuan-5.0.0-desktop-live  '5.0.0 / 2023-08-14 (Daedalus) Desktop Live DVDs' 0
add_dialog_item devuan-5.0.0-minimal-live  '5.0.0 / 2023-08-14 (Daedalus) Minimal Live CDs' 0
add_dialog_item devuan-5.0.0-installer-iso '5.0.0 / 2023-08-14 (Daedalus) Installer CDs & DVDs' 0
