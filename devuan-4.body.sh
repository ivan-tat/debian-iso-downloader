# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

devuan-4)
	DLTYPE="Devuan GNU+Linux 4.x.x / 2021-10-14 (Chimaera) Information"
	URL=$url_devuan_4
	add_dialog_item README.txt        'README.txt        | 1313' 1
	add_dialog_item Release_notes.txt 'Release_notes.txt |  15K' 1
	;;
devuan-4.0.3-desktop-live)
	DLTYPE="Devuan GNU+Linux 4.0.3 / 2023-04-30 (Chimaera) Desktop Live DVDs"
	URL=$url_devuan_4/desktop-live
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README_desktop-live.txt                      'README_desktop-live.txt | 5675' 1
	add_dialog_item devuan_chimaera_4.0.3_amd64_desktop-live.iso 'AMD64 Desktop Live DVD  | 1.2G' 0
	add_dialog_item devuan_chimaera_4.0.3_i386_desktop-live.iso  'i386 Desktop Live DVD   | 1.1G' 0
	;;
devuan-4.0.3-minimal-live)
	DLTYPE="Devuan GNU+Linux 4.0.3 / 2023-04-30 (Chimaera) Minimal Live CDs"
	URL=$url_devuan_4/minimal-live
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README_minimal-live.txt                      'README_minimal-live.txt | 6513' 1
	add_dialog_item devuan_chimaera_4.0.3_amd64_minimal-live.iso 'AMD64 Minimal Live CD   | 603M' 0
	add_dialog_item devuan_chimaera_4.0.3_i386_minimal-live.iso  'i386 Minimal Live CD    | 561M' 0
	;;
devuan-4.1.0-installer-iso)
	DLTYPE="Devuan GNU+Linux 4.1.0 / 2022-02-15 (Chimaera) Installer CDs & DVDs"
	URL=$url_devuan_4/installer-iso
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                 'README.txt                  | 1837' 1
	add_dialog_item devuan_chimaera_4.0.0_amd64_desktop.iso    'AMD64 Desktop installer DVD | 3.5G' 0
	add_dialog_item devuan_chimaera_4.0.0_amd64_server.iso     'AMD64 Server installer CD   | 642M' 0
	add_dialog_item devuan_chimaera_4.0.0_amd64_netinstall.iso 'AMD64 Network installer CD  | 372M' 0
	add_dialog_item devuan_chimaera_4.1_0_amd64_pool1.iso      'AMD64 Pool DVD #1           | 4.7G' 0
	add_dialog_item devuan_chimaera_4.0.0_amd64_cd2.iso        'AMD64 Installer CD #2       | 537M' 0
	add_dialog_item devuan_chimaera_4.0.0_amd64_cd3.iso        'AMD64 Installer CD #3       | 572M' 0
	add_dialog_item devuan_chimaera_4.0.0_amd64_cd4.iso        'AMD64 Installer CD #4       | 618M' 0
	add_dialog_item devuan_chimaera_4.0.0_i386_desktop.iso     'i386 Desktop installer DVD  | 3.5G' 0
	add_dialog_item devuan_chimaera_4.0.0_i386_server.iso      'i386 Server installer CD    | 691M' 0
	add_dialog_item devuan_chimaera_4.0.0_i386_netinstall.iso  'i386 Network installer CD   | 405M' 0
	add_dialog_item devuan_chimaera_4.1_0_i386_pool1.iso       'i386 Pool DVD #1            | 4.6G' 0
	add_dialog_item devuan_chimaera_4.0.0_i386_cd2.iso         'i386 Installer CD #2        | 543M' 0
	add_dialog_item devuan_chimaera_4.0.0_i386_cd3.iso         'i386 Installer CD #3        | 573M' 0
	add_dialog_item devuan_chimaera_4.0.0_i386_cd4.iso         'i386 Installer CD #4        | 630M' 0
	;;
