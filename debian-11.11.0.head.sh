# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023-2024 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_debian_11_11_0=$url_debian_archive
url_debian_11_11_0_fw=$url_debian_fw_archive

add_dialog_item debian-live-11.11.0-amd64-DVD         'Live 11.11.0 / 2024-08-31 (Bullseye) AMD64 DVDs' 0
add_dialog_item debian-live-11.11.0-amd64+nonfree-DVD 'Live 11.11.0 / 2024-08-31 (Bullseye) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-11.11.0-i386-DVD          'Live 11.11.0 / 2024-08-31 (Bullseye) i386  DVDs' 0
add_dialog_item debian-live-11.11.0-i386+nonfree-DVD  'Live 11.11.0 / 2024-08-31 (Bullseye) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-live-11.11.0-source            'Live 11.11.0 / 2024-08-31 (Bullseye) sources' 0
add_dialog_item debian-live-11.11.0-source+nonfree    'Live 11.11.0 / 2024-08-31 (Bullseye) sources (+non-free firmware)' 0
add_dialog_item debian-11.11.0-amd64-CD               '     11.11.0 / 2024-08-31 (Bullseye) AMD64 CDs' 0
add_dialog_item debian-11.11.0-amd64+nonfree-CD       '     11.11.0 / 2024-08-31 (Bullseye) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-11.11.0-i386-CD                '     11.11.0 / 2024-08-31 (Bullseye) i386  CDs' 0
add_dialog_item debian-11.11.0-i386+nonfree-CD        '     11.11.0 / 2024-08-31 (Bullseye) i386  CDs  (+non-free firmware)' 0
add_dialog_item debian-11.11.0-amd64-DVD              '     11.11.0 / 2024-08-31 (Bullseye) AMD64 DVDs' 0
add_dialog_item debian-11.11.0-amd64+nonfree-DVD      '     11.11.0 / 2024-08-31 (Bullseye) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-11.11.0-i386-DVD               '     11.11.0 / 2024-08-31 (Bullseye) i386  DVDs' 0
add_dialog_item debian-11.11.0-i386+nonfree-DVD       '     11.11.0 / 2024-08-31 (Bullseye) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-11.11.0-amd64-i386-CD          '     11.11.0 / 2024-08-31 (Bullseye) AMD64+i386 CDs' 0
add_dialog_item debian-11.11.0-amd64-i386+nonfree-CD  '     11.11.0 / 2024-08-31 (Bullseye) AMD64+i386 CDs (+non-free firmware)' 0
add_dialog_item debian-11.11.0-source-DVD             '     11.11.0 / 2024-08-31 (Bullseye) source DVDs' 0
