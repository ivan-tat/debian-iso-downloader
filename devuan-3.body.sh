# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

devuan-3.1.1)
	DLTYPE="Devuan GNU+Linux 3.1.1 / 2021-03-17 (Beowulf) Information"
	URL=$url_devuan_3_1_1/devuan_beowulf
	add_dialog_item README.txt        'README.txt        | 1308' 1
	add_dialog_item Release_notes.txt 'Release_notes.txt |  13K' 1
	;;
devuan-3.1.1-desktop-live)
	DLTYPE="Devuan GNU+Linux 3.1.1 / 2021-03-17 (Beowulf) Desktop Live DVDs"
	URL=$url_devuan_3_1_1/devuan_beowulf/desktop-live
	add_checksums_file SHA256 SHASUMS.txt; add_extra_file SHASUMS.txt.asc
	add_dialog_item README.desktop-live.txt                     'README.desktop-live.txt | 6124' 1
	add_dialog_item devuan_beowulf_3.1.1_amd64_desktop-live.iso 'AMD64 Desktop Live DVD  | 1.2G' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_desktop-live.iso  'i386 Desktop Live DVD   | 1.1G' 0
	;;
devuan-3.1.1-minimal-live)
	DLTYPE="Devuan GNU+Linux 3.1.1 / 2021-03-17 (Beowulf) Minimal Live CDs"
	URL=$url_devuan_3_1_1/devuan_beowulf/minimal-live
	add_checksums_file SHA256 SHASUMS.txt; add_extra_file SHASUMS.txt.asc
	add_dialog_item README.minimal-live.txt                     'README.minimal-live.txt | 6498' 1
	add_dialog_item devuan_beowulf_3.1.1_amd64_minimal-live.iso 'AMD64 Minimal Live CD   | 467M' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_minimal-live.iso  'i386 Minimal Live CD    | 464M' 0
	;;
devuan-3.1.1-installer-iso)
	DLTYPE="Devuan GNU+Linux 3.1.1 / 2021-03-17 (Beowulf) Installer CDs & DVDs"
	URL=$url_devuan_3_1_1/devuan_beowulf/installer-iso
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                'README.txt                  | 1837' 1
	add_dialog_item devuan_beowulf_3.1.1_amd64_desktop.iso    'AMD64 Desktop installer DVD | 3.6G' 0
	add_dialog_item devuan_beowulf_3.1.1_amd64_server.iso     'AMD64 Server installer CD   | 590M' 0
	add_dialog_item devuan_beowulf_3.1.1_amd64_netinstall.iso 'AMD64 Network installer CD  | 309M' 0
	add_dialog_item devuan_beowulf_3.1.0_amd64_pool1.iso      'AMD64 Pool DVD #1           | 4.5G' 0
	add_dialog_item devuan_beowulf_3.1.1_amd64_cd2.iso        'AMD64 Installer CD #2       | 604M' 0
	add_dialog_item devuan_beowulf_3.1.1_amd64_cd3.iso        'AMD64 Installer CD #3       | 628M' 0
	add_dialog_item devuan_beowulf_3.1.1_amd64_cd4.iso        'AMD64 Installer CD #4       | 565M' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_desktop.iso     'i386 Desktop installer DVD  | 3.6G' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_server.iso      'i386 Server installer CD    | 631M' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_netinstall.iso  'i386 Network installer CD   | 343M' 0
	add_dialog_item devuan_beowulf_3.1.0_i386_pool1.iso       'i386 Pool DVD #1            | 4.4G' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_cd2.iso         'i386 Installer CD #2        | 617M' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_cd3.iso         'i386 Installer CD #3        | 638M' 0
	add_dialog_item devuan_beowulf_3.1.1_i386_cd4.iso         'i386 Installer CD #4        | 578M' 0
	;;
