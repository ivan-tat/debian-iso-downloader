# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_debian_7_11_0=$url_debian_archive
url_debian_7_11_0_fw=$url_debian_fw_archive

add_dialog_item debian-live-7.11.0-amd64-DVD         'Live 7.11.0 / 2016-06-11 (Wheezy) AMD64 DVDs' 0
add_dialog_item debian-live-7.11.0-amd64+nonfree-DVD 'Live 7.11.0 / 2016-06-11 (Wheezy) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-7.11.0-i386-DVD          'Live 7.11.0 / 2016-06-11 (Wheezy) i386  DVDs' 0
add_dialog_item debian-live-7.11.0-i386+nonfree-DVD  'Live 7.11.0 / 2016-06-11 (Wheezy) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-live-7.11.0-source            'Live 7.11.0 / 2016-06-11 (Wheezy) sources' 0
add_dialog_item debian-live-7.11.0-source+nonfree    'Live 7.11.0 / 2016-06-11 (Wheezy) sources (+non-free firmware)' 0
add_dialog_item debian-7.11.0-amd64-CD               '     7.11.0 / 2016-06-05 (Wheezy) AMD64 CDs' 0
add_dialog_item debian-7.11.0-amd64+nonfree-CD       '     7.11.0 / 2016-06-05 (Wheezy) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-7.11.0-i386-CD                '     7.11.0 / 2016-06-05 (Wheezy) i386  CDs' 0
add_dialog_item debian-7.11.0-i386+nonfree-CD        '     7.11.0 / 2016-06-05 (Wheezy) i386  CDs  (+non-free firmware)' 0
add_dialog_item debian-7.11.0-amd64-DVD              '     7.11.0 / 2016-06-05 (Wheezy) AMD64 DVDs' 0
add_dialog_item debian-7.11.0-i386-DVD               '     7.11.0 / 2016-06-05 (Wheezy) i386  DVDs' 0
add_dialog_item debian-7.11.0-amd64-i386-CD          '     7.11.0 / 2016-06-05 (Wheezy) AMD64+i386 CDs' 0
add_dialog_item debian-7.11.0-amd64-i386+nonfree-CD  '     7.11.0 / 2016-06-05 (Wheezy) AMD64+i386 CDs (+non-free firmware)' 0
add_dialog_item debian-7.11.0-amd64-i386-DVD         '     7.11.0 / 2016-06-05 (Wheezy) AMD64+i386 DVDs' 0
add_dialog_item debian-7.11.0-source-CD              '     7.11.0 / 2016-06-06 (Wheezy) source CDs' 0
add_dialog_item debian-7.11.0-source-DVD             '     7.11.0 / 2016-06-05 (Wheezy) source DVDs' 0
