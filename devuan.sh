#!/bin/bash -e
# Devuan ISO downloader
# Version 1.0
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

source ./common.shlib

url_devuan_release=https://files.devuan.org
url_devuan_archive=https://files.devuan.org/archive

echo "" >./devuan.tmp
cat\
 devuan.head.sh\
 devuan-1.head.sh\
 devuan-2.head.sh\
 devuan-3.head.sh\
 devuan-4.head.sh\
 devuan-5.head.sh\
 >>./devuan.tmp
source ./devuan.tmp
rm -f ./devuan.tmp

select_category DLTYPE 'Devuan GNU+Linux downloader' 'Choose a version to download:'

OUTPUT=$DLTYPE

cat >./devuan.tmp <<EOT
case "$DLTYPE" in
EOT
cat\
 devuan.body.sh\
 devuan-1.body.sh\
 devuan-2.body.sh\
 devuan-3.body.sh\
 devuan-4.body.sh\
 devuan-5.body.sh\
 >>./devuan.tmp
cat >>./devuan.tmp <<EOT
*)
	echo 'Unknown option selected.' >&2
	exit 1
	;;
esac
EOT
source ./devuan.tmp
rm -f ./devuan.tmp

select_and_download_files "$DLTYPE" "$URL" "$OUTPUT"

cd "$OUTPUT"
check_sums
cd "$OLDPWD"

echo 'Done.' >&2
