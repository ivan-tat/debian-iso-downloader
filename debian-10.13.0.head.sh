# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_debian_10_13_0=$url_debian_archive
url_debian_10_13_0_fw=$url_debian_fw_archive

add_dialog_item debian-live-10.13.0-amd64-DVD         'Live 10.13.0 / 2022-09-10 (Buster) AMD64 DVDs' 0
add_dialog_item debian-live-10.13.0-amd64+nonfree-DVD 'Live 10.13.0 / 2022-09-10 (Buster) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-10.13.0-i386-DVD          'Live 10.13.0 / 2022-09-10 (Buster) i386  DVDs' 0
add_dialog_item debian-live-10.13.0-i386+nonfree-DVD  'Live 10.13.0 / 2022-09-10 (Buster) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-live-10.13.0-source            'Live 10.13.0 / 2022-09-10 (Buster) sources' 0
add_dialog_item debian-live-10.13.0-source+nonfree    'Live 10.13.0 / 2022-09-10 (Buster) sources (+non-free firmware)' 0
add_dialog_item debian-10.13.0-amd64-CD               '     10.13.0 / 2022-09-10 (Buster) AMD64 CDs' 0
add_dialog_item debian-10.13.0-amd64+nonfree-CD       '     10.13.0 / 2022-09-10 (Buster) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-10.13.0-i386-CD                '     10.13.0 / 2022-09-10 (Buster) i386  CDs' 0
add_dialog_item debian-10.13.0-i386+nonfree-CD        '     10.13.0 / 2022-09-10 (Buster) i386  CDs  (+non-free firmware)' 0
add_dialog_item debian-10.13.0-amd64-DVD              '     10.13.0 / 2022-09-10 (Buster) AMD64 DVDs' 0
add_dialog_item debian-10.13.0-amd64+nonfree-DVD      '     10.13.0 / 2022-09-10 (Buster) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-10.13.0-i386-DVD               '     10.13.0 / 2022-09-10 (Buster) i386  DVDs' 0
add_dialog_item debian-10.13.0-i386+nonfree-DVD       '     10.13.0 / 2022-09-10 (Buster) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-10.13.0-amd64-i386-CD          '     10.13.0 / 2022-09-10 (Buster) AMD64+i386 CDs' 0
add_dialog_item debian-10.13.0-amd64-i386+nonfree-CD  '     10.13.0 / 2022-09-10 (Buster) AMD64+i386 CDs (+non-free firmware)' 0
add_dialog_item debian-10.13.0-source-DVD             '     10.13.0 / 2022-09-10 (Buster) source DVDs' 0
