# Debian-ISO-downloader

Simple script to download Debian's and Devuan's ISO images (released into the [public domain](LICENSES/Unlicense.txt))

## Hint on changing URLs when placing a Debian release into the archive

Official Debian releases (non-free firmware included since 2023/02/19):

`https://cdimage.debian.org/cdimage/release/...`<br/>
into<br/>
`https://cdimage.debian.org/cdimage/archive/...`

Unofficial Debian releases with non-free firmwares (before 2023/02/19):

`https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/...`<br/>
into<br/>
`https://cdimage.debian.org/cdimage/unofficial/non-free/cd-including-firmware/archive/...`

# References

* [REUSE SOFTWARE](https://reuse.software/) - a set of recommendations to make licensing your projects easier
* [The Software Package Data Exchange (SPDX)](https://spdx.dev/) - an open standard for communicating software bill of material information, including components, licenses, copyrights, and security references
* [GNU Operating System](https://www.gnu.org/)
* [Debian](https://www.debian.org/) - a complete free operating system
* [Devuan](https://www.devuan.org/) - a fork of Debian without systemd
* [GNU Standards](http://savannah.gnu.org/projects/gnustandards) ([package](https://pkgs.org/search/?q=gnu-standards)) - GNU coding and package maintenance standards
* [GNU Core Utilities](https://www.gnu.org/software/coreutils/) ([package](https://pkgs.org/search/?q=coreutils))
* [GNU Bash](https://www.gnu.org/software/bash/) ([package](https://pkgs.org/search/?q=bash)) - GNU Bourne Again SHell
* [whiptail](https://pagure.io/newt) ([package](https://pkgs.org/search/?q=whiptail)) - a utility to display user-friendly dialog boxes from shell scripts
* [dialog](https://invisible-island.net/dialog/dialog.html) ([package](https://pkgs.org/search/?q=dialog)) - a utility to display user-friendly dialog boxes from shell scripts
* [GNU Wget](https://www.gnu.org/software/wget/) ([package](https://pkgs.org/search/?q=wget)) - a free software for retrieving files using HTTP, HTTPS, FTP and FTPS
* [curl](https://curl.se/) ([package](https://pkgs.org/search/?q=curl)) - command line tool and library for transferring data with URL syntax
* [Midnight Commander](https://www.midnight-commander.org) ([package](https://pkgs.org/search/?q=mc)) - a free text-mode full-screen file manager
* [GNU GRUB (GRand Unified Bootloader)](https://www.gnu.org/software/grub/) ([package](https://pkgs.org/search/?q=grub2)) - a free Multiboot boot loader
* [Pendrivelinux.com](https://www.pendrivelinux.com/) - Boot and Run Linux from Bootable USB
* [Ventoy](https://www.ventoy.net/en/index.html) - an open source tool to create bootable USB drive for ISO/WIM/IMG/VHD(x)/EFI files
