# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_devuan_2_0_0=$url_devuan_archive
url_devuan_2_0_0_virtual=$url_devuan_release
url_devuan_2_1=$url_devuan_release

add_dialog_item devuan-2.1                 '2.1   / 2019-09-22 (ASCII) Information' 0
add_dialog_item devuan-2.0.0-desktop-live  '2.0.0 / 2018-06-06 (ASCII) Desktop Live DVDs' 0
add_dialog_item devuan-2.1-desktop-live    '2.1   / 2019-09-22 (ASCII) Desktop Live DVDs' 0
add_dialog_item devuan-2.0.0-minimal-live  '2.0.0 / 2018-06-06 (ASCII) Minimal Live CDs' 0
add_dialog_item devuan-2.1-minimal-live    '2.1   / 2019-09-22 (ASCII) Minimal Live CDs' 0
add_dialog_item devuan-2.0.0-installer-iso '2.0.0 / 2018-06-06 (ASCII) Installer CDs & DVDs' 0
add_dialog_item devuan-2.1-installer-iso   '2.1   / 2019-09-22 (ASCII) Installer CDs & DVDs' 0
add_dialog_item devuan-2.0.0-virtual       '2.0.0 / 2018-06-06 (ASCII) Virtual images' 0
