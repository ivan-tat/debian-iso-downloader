# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

devuan-2.1)
	DLTYPE="Devuan GNU+Linux 2.1 / 2019-09-22 (ASCII) Information"
	URL=$url_devuan_2_1/devuan_ascii
	add_dialog_item README.txt        'README.txt        | 1485' 1
	add_dialog_item Release_notes.txt 'Release_notes.txt |  12K' 1
	;;
devuan-2.0.0-desktop-live)
	DLTYPE="Devuan GNU+Linux 2.0.0 / 2018-06-06 (ASCII) Desktop Live DVDs"
	URL=$url_devuan_2_0_0/devuan_ascii/desktop-live
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                     'README.txt             | 5775' 1
	add_dialog_item devuan_ascii_2.0.0_amd64_desktop-live.iso      'AMD64 Desktop Live DVD | 997M' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_desktop-live.list.txt ' * .list.txt file      |  42K' 0
	add_dialog_item devuan_ascii_2.0.0_i386_desktop-live.iso       'i386 Desktop Live DVD  | 1.0G' 0
	add_dialog_item devuan_ascii_2.0.0_i386_desktop-live.list.txt  ' * .list.txt file      |  41K' 0
	;;
devuan-2.1-desktop-live)
	DLTYPE="Devuan GNU+Linux 2.1 / 2019-09-22 (ASCII) Desktop Live DVDs"
	URL=$url_devuan_2_1/devuan_ascii/desktop-live
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README.desktop-live.txt                 'README.desktop-live.txt | 6300' 1
	add_dialog_item devuan_ascii_2.1_amd64_desktop-live.iso 'AMD64 Desktop Live DVD  | 1.0G' 0
	add_dialog_item devuan_ascii_2.1_i386_desktop-live.iso  'i386 Desktop Live DVD   | 1.0G' 0
	;;
devuan-2.0.0-minimal-live)
	DLTYPE="Devuan GNU+Linux 2.0.0 / 2018-06-06 (ASCII) Minimal Live CDs"
	URL=$url_devuan_2_0_0/devuan_ascii/minimal-live
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                                'README.txt            | 6493' 1
	add_dialog_item devuan_ascii_2.0.0_amd64_minimal-live.iso 'AMD64 Minimal Live CD | 363M' 0
	add_dialog_item devuan_ascii_2.0.0_i386_minimal-live.iso  'i386 Minimal Live CD  | 368M' 0
	;;
devuan-2.1-minimal-live)
	DLTYPE="Devuan GNU+Linux 2.1 / 2019-09-22 (ASCII) Minimal Live CDs"
	URL=$url_devuan_2_1/devuan_ascii/minimal-live
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README_ASCII.txt                        'README_ASCII.txt      | 6494' 1
	add_dialog_item devuan_ascii_2.1_amd64_minimal-live.iso 'AMD64 Minimal Live CD | 399M' 0
	add_dialog_item devuan_ascii_2.1_i386_minimal-live.iso  'i386 Minimal Live CD  | 404M' 0
	;;
devuan-2.0.0-installer-iso)
	DLTYPE="Devuan GNU+Linux 2.0.0 / 2018-06-06 (ASCII) Installer CDs & DVDs"
	URL=$url_devuan_2_0_0/devuan_ascii/installer-iso
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                           'README.txt                 | 1597' 1
	add_dialog_item devuan_ascii_2.0.0_amd64_cd-1.iso    'AMD64 Installer CD #1      | 647M' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_cd-2.iso    'AMD64 Installer CD #2      | 647M' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_cd-3.iso    'AMD64 Installer CD #3      | 648M' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_dvd-1.iso   'AMD64 Installer DVD #1     | 4.4G' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_netinst.iso 'AMD64 Network installer CD | 298M' 0
	add_dialog_item devuan_ascii_2.0.0_i386_cd-1.iso     'i386 Installer CD #1       | 646M' 0
	add_dialog_item devuan_ascii_2.0.0_i386_cd-2.iso     'i386 Installer CD #2       | 647M' 0
	add_dialog_item devuan_ascii_2.0.0_i386_cd-3.iso     'i386 Installer CD #3       | 645M' 0
	add_dialog_item devuan_ascii_2.0.0_i386_dvd-1.iso    'i386 Installer DVD #1      | 4.4G' 0
	add_dialog_item devuan_ascii_2.0.0_i386_netinst.iso  'i386 Network installer CD  | 342M' 0
	;;
devuan-2.1-installer-iso)
	DLTYPE="Devuan GNU+Linux 2.1 / 2019-09-22 (ASCII) Installer CDs & DVDs"
	URL=$url_devuan_2_1/devuan_ascii/installer-iso
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                         'README.txt                 | 1591' 1
	add_dialog_item devuan_ascii_2.1_amd64_cd-1.iso    'AMD64 Installer CD #1      | 669M' 0
	add_dialog_item devuan_ascii_2.1_amd64_cd-2.iso    'AMD64 Installer CD #2      | 654M' 0
	add_dialog_item devuan_ascii_2.1_amd64_cd-3.iso    'AMD64 Installer CD #3      | 653M' 0
	add_dialog_item devuan_ascii_2.1_amd64_dvd-1.iso   'AMD64 Installer DVD #1     | 4.4G' 0
	add_dialog_item devuan_ascii_2.1_amd64_netinst.iso 'AMD64 Network installer CD | 305M' 0
	add_dialog_item devuan_ascii_2.1_i386_cd-1.iso     'i386 Installer CD #1       | 672M' 0
	add_dialog_item devuan_ascii_2.1_i386_cd-2.iso     'i386 Installer CD #2       | 653M' 0
	add_dialog_item devuan_ascii_2.1_i386_cd-3.iso     'i386 Installer CD #3       | 647M' 0
	add_dialog_item devuan_ascii_2.1_i386_dvd-1.iso    'i386 Installer DVD #1      | 4.4G' 0
	add_dialog_item devuan_ascii_2.1_i386_netinst.iso  'i386 Network installer CD  | 349M' 0
	;;
devuan-2.0.0-virtual)
	DLTYPE="Devuan GNU+Linux 2.0.0 / 2018-06-06 (ASCII) Virtual images"
	URL=$url_devuan_2_0_0_virtual/devuan_ascii/virtual
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.asc
	add_dialog_item README.txt                             'README.txt                         |  646' 1
	add_dialog_item devuan_ascii_2.0.0_amd64_qemu.qcow2.xz 'AMD64 QEMU qcow2 image (.qcow2.xz) | 532M' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_vagrant.box   'AMD64 Vagrant image (.box)         | 707M' 0
	add_dialog_item Vagrantfile                            ' * Vagrantfile                     |  288' 0
	add_dialog_item devuan_ascii_2.0.0_amd64_vbox.vdi.xz   'AMD64 VirtualBox VDI (.vdi.xz)     | 533M' 0
	;;
