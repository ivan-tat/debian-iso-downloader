# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

add_dialog_item devuan          'Information and keys' 0
add_dialog_item devuan-torrents 'Torrent files' 0
