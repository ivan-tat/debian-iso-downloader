# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

add_dialog_item debian-live-testing-weekly-amd64+nonfree-DVD  'Live testing (weekly builds) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-testing-weekly-source             'Live testing (weekly builds) sources' 0
add_dialog_item debian-testing-daily-amd64+nonfree-CD         '     testing (daily builds)  AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-testing-daily-amd64+sid_d-i+nonfree-CD '     testing (daily builds)  AMD64 CDs  (+Sid D-I, +non-free firmware)' 0
add_dialog_item debian-testing-weekly-amd64+nonfree-CD        '     testing (weekly builds) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-testing-weekly-amd64+nonfree-DVD       '     testing (weekly builds) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-testing-weekly-source-DVD              '     testing (weekly builds) source DVDs' 0
