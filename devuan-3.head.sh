# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_devuan_3_1_1=$url_devuan_release

add_dialog_item devuan-3.1.1               '3.1.1 / 2021-03-21 (Beowulf) Information' 0
add_dialog_item devuan-3.1.1-desktop-live  '3.1.1 / 2021-03-21 (Beowulf) Desktop Live DVDs' 0
add_dialog_item devuan-3.1.1-minimal-live  '3.1.1 / 2021-03-21 (Beowulf) Minimal Live CDs' 0
add_dialog_item devuan-3.1.1-installer-iso '3.1.1 / 2021-03-21 (Beowulf) Installer CDs & DVDs' 0
