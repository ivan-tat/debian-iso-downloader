# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_debian_9_13_0=$url_debian_archive
url_debian_9_13_0_fw=$url_debian_fw_archive

add_dialog_item debian-live-9.13.0-amd64-DVD         'Live 9.13.0 / 2020-07-18 (Stretch) AMD64 DVDs' 0
add_dialog_item debian-live-9.13.0-amd64+nonfree-DVD 'Live 9.13.0 / 2020-07-18 (Stretch) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-9.13.0-i386-DVD          'Live 9.13.0 / 2020-07-18 (Stretch) i386  DVDs' 0
add_dialog_item debian-live-9.13.0-i386+nonfree-DVD  'Live 9.13.0 / 2020-07-18 (Stretch) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-live-9.13.0-source            'Live 9.13.0 / 2020-07-18 (Stretch) sources' 0
add_dialog_item debian-live-9.13.0-source+nonfree    'Live 9.13.0 / 2020-07-18 (Stretch) sources (+non-free firmware)' 0
add_dialog_item debian-9.13.0-amd64-CD               '     9.13.0 / 2020-07-18 (Stretch) AMD64 CDs' 0
add_dialog_item debian-9.13.0-amd64+nonfree-CD       '     9.13.0 / 2020-07-18 (Stretch) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-9.13.0-i386-CD                '     9.13.0 / 2020-07-18 (Stretch) i386  CDs' 0
add_dialog_item debian-9.13.0-i386+nonfree-CD        '     9.13.0 / 2020-07-18 (Stretch) i386  CDs  (+non-free firmware)' 0
add_dialog_item debian-9.13.0-amd64-DVD              '     9.13.0 / 2020-07-18 (Stretch) AMD64 DVDs' 0
add_dialog_item debian-9.13.0-amd64+nonfree-DVD      '     9.13.0 / 2020-07-18 (Stretch) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-9.13.0-i386-DVD               '     9.13.0 / 2020-07-18 (Stretch) i386  DVDs' 0
add_dialog_item debian-9.13.0-i386+nonfree-DVD       '     9.13.0 / 2020-07-18 (Stretch) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-9.13.0-amd64-i386-CD          '     9.13.0 / 2020-07-18 (Stretch) AMD64+i386 CDs' 0
add_dialog_item debian-9.13.0-amd64-i386+nonfree-CD  '     9.13.0 / 2020-07-18 (Stretch) AMD64+i386 CDs (+non-free firmware)' 0
add_dialog_item debian-9.13.0-source-DVD             '     9.13.0 / 2020-07-18 (Stretch) source DVDs' 0
