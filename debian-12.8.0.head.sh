# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023-2024 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_debian_12_8_0=$url_debian_release

add_dialog_item debian-live-12.8.0-amd64+nonfree-DVD 'Live 12.8.0 / 2024-11-09 (Bookworm) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-12.8.0-source+nonfree    'Live 12.8.0 / 2024-11-09 (Bookworm) source     (+non-free firmware)' 0
add_dialog_item debian-12.8.0-amd64+nonfree-CD       '     12.8.0 / 2024-11-09 (Bookworm) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-12.8.0-i386+nonfree-CD        '     12.8.0 / 2024-11-09 (Bookworm) i386  CDs  (+non-free firmware)' 0
add_dialog_item debian-12.8.0-amd64+nonfree-DVD      '     12.8.0 / 2024-11-09 (Bookworm) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-12.8.0-i386+nonfree-DVD       '     12.8.0 / 2024-11-09 (Bookworm) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-12.8.0-source-DVD             '     12.8.0 / 2024-11-09 (Bookworm) source DVDs' 0
