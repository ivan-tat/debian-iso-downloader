# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

debian-live-7.11.0-amd64-DVD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2016-06-11 (Wheezy) AMD64 DVDs"
	URL=$url_debian_7_11_0/$VERSION-live/amd64/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso          'GNOME desktop live DVD    | 1.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.contents ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.log      ' * .iso.log file          | 630K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.packages ' * .iso.packages file     |  42K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.zsync    ' * .iso.zsync file        | 4.8M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso            'KDE desktop live DVD      | 1.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.contents   ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.log        ' * .iso.log file          | 592K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.packages   ' * .iso.packages file     |  39K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.zsync      ' * .iso.zsync file        | 4.9M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso           'LXDE desktop live DVD     | 896M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.contents  ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.log       ' * .iso.log file          | 491K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.packages  ' * .iso.packages file     |  30K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.zsync     ' * .iso.zsync file        | 3.5M' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue.iso                 'Rescue live DVD           | 685M' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue.iso.contents        ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue.iso.log             ' * .iso.log file          | 462K' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue.iso.packages        ' * .iso.packages file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue.iso.zsync           ' * .iso.zsync file        | 2.3M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso               'Standard desktop live DVD | 465M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.contents      ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.log           ' * .iso.log file          | 300K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.packages      ' * .iso.packages file     |  15K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.zsync         ' * .iso.zsync file        | 1.6M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso           'Xfce desktop live DVD     | 968M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.contents  ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.log       ' * .iso.log file          | 528K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.packages  ' * .iso.packages file     |  34K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.zsync     ' * .iso.zsync file        | 3.8M' 0
	;;
debian-live-7.11.0-amd64+nonfree-DVD)
	VERSION=7.11.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2016-06-11 (Wheezy) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_7_11_0_fw/$VERSION-live+nonfree/amd64/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso          'GNOME desktop live DVD    | 1.3G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.contents ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.log      ' * .iso.log file          | 709K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.packages ' * .iso.packages file     |  44K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.zsync    ' * .iso.zsync file        | 5.1M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso            'KDE desktop live DVD      | 1.3G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.contents   ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.log        ' * .iso.log file          | 670K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.packages   ' * .iso.packages file     |  41K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.zsync      ' * .iso.zsync file        | 5.3M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso           'LXDE desktop live DVD     | 1.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.contents  ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.log       ' * .iso.log file          | 569K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.packages  ' * .iso.packages file     |  32K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.zsync     ' * .iso.zsync file        | 3.9M' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue+nonfree.iso                 'Rescue live DVD           | 723M' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue+nonfree.iso.contents        ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue+nonfree.iso.log             ' * .iso.log file          | 525K' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue+nonfree.iso.packages        ' * .iso.packages file     |  26K' 0
	add_dialog_item debian-live-$VERSION-amd64-rescue+nonfree.iso.zsync           ' * .iso.zsync file        | 2.5M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso               'Standard desktop live DVD | 500M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.contents      ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.log           ' * .iso.log file          | 361K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.packages      ' * .iso.packages file     |  16K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.zsync         ' * .iso.zsync file        | 1.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso           'Xfce desktop live DVD     | 1.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.contents  ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.log       ' * .iso.log file          | 606K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.packages  ' * .iso.packages file     |  35K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.zsync     ' * .iso.zsync file        | 4.1M' 0
	;;
debian-live-7.11.0-i386-DVD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2016-06-11 (Wheezy) i386 DVDs"
	URL=$url_debian_7_11_0/$VERSION-live/i386/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso          'GNOME desktop live DVD    | 1.3G' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.contents ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.log      ' * .iso.log file          | 1.6M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.packages ' * .iso.packages file     |  42K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.zsync    ' * .iso.zsync file        | 5.1M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso            'KDE desktop live DVD      | 1.3G' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.contents   ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.log        ' * .iso.log file          | 1.4M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.packages   ' * .iso.packages file     |  39K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.zsync      ' * .iso.zsync file        | 5.2M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso           'LXDE desktop live DVD     | 1.0G' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.contents  ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.log       ' * .iso.log file          | 1.2M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.packages  ' * .iso.packages file     |  30K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.zsync     ' * .iso.zsync file        | 3.8M' 0
	add_dialog_item debian-live-$VERSION-i386-rescue.iso                 'Rescue live DVD           | 762M' 0
	add_dialog_item debian-live-$VERSION-i386-rescue.iso.contents        ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-i386-rescue.iso.log             ' * .iso.log file          | 1.0M' 0
	add_dialog_item debian-live-$VERSION-i386-rescue.iso.packages        ' * .iso.packages file     |  25K' 0
	add_dialog_item debian-live-$VERSION-i386-rescue.iso.zsync           ' * .iso.zsync file        | 2.6M' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso               'Standard desktop live DVD | 540M' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.contents      ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.log           ' * .iso.log file          | 657K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.packages      ' * .iso.packages file     |  15K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.zsync         ' * .iso.zsync file        | 1.8M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso           'Xfce desktop live DVD     | 1.0G' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.contents  ' * .iso.contents file     |  29K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.log       ' * .iso.log file          | 1.3M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.packages  ' * .iso.packages file     |  34K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.zsync     ' * .iso.zsync file        | 4.1M' 0
	;;
debian-live-7.11.0-i386+nonfree-DVD)
	VERSION=7.11.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2016-06-11 (Wheezy) i386 DVDs (+non-free firmware)"
	URL=$url_debian_7_11_0_fw/$VERSION-live+nonfree/i386/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso          'GNOME desktop live DVD    | 1.4G' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.contents ' * .iso.contents file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.log      ' * .iso.log file          | 1.7M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.packages ' * .iso.packages file     |  44K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.zsync    ' * .iso.zsync file        | 5.5M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso            'KDE desktop live DVD      | 1.4G' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.contents   ' * .iso.contents file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.log        ' * .iso.log file          | 1.5M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.packages   ' * .iso.packages file     |  41K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.zsync      ' * .iso.zsync file        | 5.6M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso           'LXDE desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.contents  ' * .iso.contents file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.log       ' * .iso.log file          | 1.3M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.packages  ' * .iso.packages file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.zsync     ' * .iso.zsync file        | 4.3M' 0
	add_dialog_item debian-live-$VERSION-i386-rescue+nonfree.iso                 'Rescue live DVD           | 808M' 0
	add_dialog_item debian-live-$VERSION-i386-rescue+nonfree.iso.contents        ' * .iso.contents file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-rescue+nonfree.iso.log             ' * .iso.log file          | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-rescue+nonfree.iso.packages        ' * .iso.packages file     |  26K' 0
	add_dialog_item debian-live-$VERSION-i386-rescue+nonfree.iso.zsync           ' * .iso.zsync file        | 3.2M' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso               'Standard desktop live DVD | 584M' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.contents      ' * .iso.contents file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.log           ' * .iso.log file          | 732K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.packages      ' * .iso.packages file     |  16K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.zsync         ' * .iso.zsync file        | 2.0M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso           'Xfce desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.contents  ' * .iso.contents file     |  32K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.log       ' * .iso.log file          | 1.4M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.packages  ' * .iso.packages file     |  35K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.zsync     ' * .iso.zsync file        | 4.5M' 0
	;;
debian-live-7.11.0-source)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2016-06-11 (Wheezy) sources"
	URL=$url_debian_7_11_0/$VERSION-live/source/tar
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-gnome-desktop.debian.tar      'GNOME desktop live source .tar file    | 2.8G' 0
	add_dialog_item debian-live-$VERSION-source-gnome-desktop.debian.contents ' * .contents file                      | 141K' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop.debian.tar        'KDE desktop live source .tar file      | 3.4G' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop.debian.contents   ' * .contents file                      | 125K' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop.debian.tar       'LXDE desktop live source .tar file     | 2.3G' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop.debian.contents  ' * .contents file                      | 111K' 0
	add_dialog_item debian-live-$VERSION-source-rescue.debian.tar             'Rescue live source .tar file           | 1.3G' 0
	add_dialog_item debian-live-$VERSION-source-rescue.debian.contents        ' * .contents file                      |  97K' 0
	add_dialog_item debian-live-$VERSION-source-standard.debian.tar           'Standard desktop live source .tar file | 923M' 0
	add_dialog_item debian-live-$VERSION-source-standard.debian.contents      ' * .contents file                      |  51K' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop.debian.tar       'Xfce desktop live source .tar file     | 2.6G' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop.debian.contents  ' * .contents file                      | 126K' 0
	;;
debian-live-7.11.0-source+nonfree)
	VERSION=7.11.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2016-06-11 (Wheezy) sources (+non-free firmware)"
	URL=$url_debian_7_11_0_fw/$VERSION-live+nonfree/source/tar
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-gnome-desktop+nonfree.debian.tar      'GNOME desktop live source .tar file    | 3.2G' 0
	add_dialog_item debian-live-$VERSION-source-gnome-desktop+nonfree.debian.contents ' * .contents file                      | 145K' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop+nonfree.debian.tar        'KDE desktop live source .tar file      | 3.6G' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop+nonfree.debian.contents   ' * .contents file                      | 128K' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop+nonfree.debian.tar       'LXDE desktop live source .tar file     | 2.7G' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop+nonfree.debian.contents  ' * .contents file                      | 115K' 0
	add_dialog_item debian-live-$VERSION-source-rescue+nonfree.debian.tar             'Rescue live source .tar file           | 1.4G' 0
	add_dialog_item debian-live-$VERSION-source-rescue+nonfree.debian.contents        ' * .contents file                      |  99K' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.debian.tar           'Standard desktop live source .tar file | 938M' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.debian.contents      ' * .contents file                      |  52K' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop+nonfree.debian.tar       'Xfce desktop live source .tar file     | 2.8G' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop+nonfree.debian.contents  ' * .contents file                      | 129K' 0
	;;
debian-7.11.0-amd64-CD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) AMD64 CDs"
	URL=$url_debian_7_11_0/$VERSION/amd64/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-CD-1.iso         'Installation CD #1              | 648M' 0
	add_dialog_item debian-$VERSION-amd64-CD-2.iso         'Installation CD #2              | 639M' 0
	add_dialog_item debian-$VERSION-amd64-CD-3.iso         'Installation CD #3              | 647M' 0
	add_dialog_item debian-$VERSION-amd64-CD-4.iso         'Installation CD #4              | 615M' 0
	add_dialog_item debian-$VERSION-amd64-CD-5.iso         'Installation CD #5              | 611M' 0
	add_dialog_item debian-$VERSION-amd64-CD-6.iso         'Installation CD #6              | 648M' 0
	add_dialog_item debian-$VERSION-amd64-CD-7.iso         'Installation CD #7              | 645M' 0
	add_dialog_item debian-$VERSION-amd64-CD-8.iso         'Installation CD #8              | 648M' 0
	add_dialog_item debian-$VERSION-amd64-kde-CD-1.iso     'KDE desktop installation CD #1  | 634M' 0
	add_dialog_item debian-$VERSION-amd64-lxde-CD-1.iso    'LXDE desktop installation CD #1 | 647M' 0
	add_dialog_item debian-$VERSION-amd64-netinst.iso      'Network installation CD         | 222M' 0
	add_dialog_item debian-$VERSION-amd64-xfce-CD-1.iso    'Xfce desktop installation CD #1 | 620M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-1.iso  'Update CD #1                    | 208M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-2.iso  'Update CD #2                    | 623M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-3.iso  'Update CD #3                    | 626M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-4.iso  'Update CD #4                    | 645M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-5.iso  'Update CD #5                    | 566M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-6.iso  'Update CD #6                    | 630M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-7.iso  'Update CD #7                    | 471M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-8.iso  'Update CD #8                    | 580M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-9.iso  'Update CD #9                    | 643M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-10.iso 'Update CD #10                   | 642M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-11.iso 'Update CD #11                   | 641M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-12.iso 'Update CD #12                   | 643M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-13.iso 'Update CD #13                   | 637M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-14.iso 'Update CD #14                   | 638M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-15.iso 'Update CD #15                   | 574M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-16.iso 'Update CD #16                   | 590M' 0
	add_dialog_item debian-update-$VERSION-amd64-CD-17.iso 'Update CD #17                   | 224M' 0
	;;
debian-7.11.0-amd64+nonfree-CD)
	VERSION=7.11.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) AMD64 CDs (+non-free firmware)"
	URL=$url_debian_7_11_0_fw/$VERSION+nonfree/amd64/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-netinst.iso 'Network installation CD | 236M' 0
	;;
debian-7.11.0-i386-CD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) i386 CDs"
	URL=$url_debian_7_11_0/$VERSION/i386/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-CD-1.iso         'Installation CD #1              | 626M' 0
	add_dialog_item debian-$VERSION-i386-CD-2.iso         'Installation CD #2              | 648M' 0
	add_dialog_item debian-$VERSION-i386-CD-3.iso         'Installation CD #3              | 648M' 0
	add_dialog_item debian-$VERSION-i386-CD-4.iso         'Installation CD #4              | 648M' 0
	add_dialog_item debian-$VERSION-i386-CD-5.iso         'Installation CD #5              | 647M' 0
	add_dialog_item debian-$VERSION-i386-CD-6.iso         'Installation CD #6              | 647M' 0
	add_dialog_item debian-$VERSION-i386-CD-7.iso         'Installation CD #7              | 636M' 0
	add_dialog_item debian-$VERSION-i386-CD-8.iso         'Installation CD #8              | 648M' 0
	add_dialog_item debian-$VERSION-i386-kde-CD-1.iso     'KDE desktop installation CD #1  | 648M' 0
	add_dialog_item debian-$VERSION-i386-lxde-CD-1.iso    'LXDE desktop installation CD #1 | 647M' 0
	add_dialog_item debian-$VERSION-i386-netinst.iso      'Network installation CD         | 277M' 0
	add_dialog_item debian-$VERSION-i386-xfce-CD-1.iso    'Xfce desktop installation CD #1 | 647M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-1.iso  'Update CD #1                    | 204M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-2.iso  'Update CD #2                    | 611M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-3.iso  'Update CD #3                    | 624M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-4.iso  'Update CD #4                    | 645M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-5.iso  'Update CD #5                    | 533M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-6.iso  'Update CD #6                    | 638M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-7.iso  'Update CD #7                    | 537M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-8.iso  'Update CD #8                    | 599M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-9.iso  'Update CD #9                    | 644M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-10.iso 'Update CD #10                   | 640M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-11.iso 'Update CD #11                   | 644M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-12.iso 'Update CD #12                   | 635M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-13.iso 'Update CD #13                   | 459M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-14.iso 'Update CD #14                   | 642M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-15.iso 'Update CD #15                   | 645M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-16.iso 'Update CD #16                   | 643M' 0
	add_dialog_item debian-update-$VERSION-i386-CD-17.iso 'Update CD #17                   | 343M' 0
	;;
debian-7.11.0-i386+nonfree-CD)
	VERSION=7.11.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) i386 CDs (+non-free firmware)"
	URL=$url_debian_7_11_0_fw/$VERSION+nonfree/i386/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-i386-netinst.iso 'Network installation CD | 291M' 0
	;;
debian-7.11.0-amd64-DVD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) AMD64 DVDs"
	URL=$url_debian_7_11_0/$VERSION/amd64/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-DVD-1.iso        'Installation DVD #1 | 3.7G' 0
	add_dialog_item debian-$VERSION-amd64-DVD-2.iso        'Installation DVD #2 | 4.2G' 0
	add_dialog_item debian-$VERSION-amd64-DVD-3.iso        'Installation DVD #3 | 4.3G' 0
	add_dialog_item debian-update-$VERSION-amd64-DVD-1.iso 'Update DVD #1       | 4.2G' 0
	add_dialog_item debian-update-$VERSION-amd64-DVD-2.iso 'Update DVD #2       | 4.2G' 0
	add_dialog_item debian-update-$VERSION-amd64-DVD-3.iso 'Update DVD #3       | 1.0G' 0
	;;
debian-7.11.0-i386-DVD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) i386 DVDs"
	URL=$url_debian_7_11_0/$VERSION/i386/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-DVD-1.iso        'Installation DVD #1 | 3.7G' 0
	add_dialog_item debian-$VERSION-i386-DVD-2.iso        'Installation DVD #2 | 4.2G' 0
	add_dialog_item debian-$VERSION-i386-DVD-3.iso        'Installation DVD #3 | 4.3G' 0
	add_dialog_item debian-update-$VERSION-i386-DVD-1.iso 'Update DVD #1       | 4.2G' 0
	add_dialog_item debian-update-$VERSION-i386-DVD-2.iso 'Update DVD #2       | 4.2G' 0
	add_dialog_item debian-update-$VERSION-i386-DVD-3.iso 'Update DVD #3       | 1.1G' 0
	;;
debian-7.11.0-amd64-i386-CD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) i386+AMD64 CDs"
	URL=$url_debian_7_11_0/$VERSION/multi-arch/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-i386-netinst.iso 'Network installation CD | 485M' 0
	;;
debian-7.11.0-amd64-i386+nonfree-CD)
	VERSION=7.11.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) i386+AMD64 CDs (+non-free firmware)"
	URL=$url_debian_7_11_0_fw/$VERSION+nonfree/multi-arch/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-i386-netinst.iso 'Network installation CD | 500M' 0
	;;
debian-7.11.0-amd64-i386-DVD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) i386+AMD64 DVDs"
	URL=$url_debian_7_11_0/$VERSION/multi-arch/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-amd64-source-DVD-1.iso 'Source DVD #1 | 3.7G' 0
	;;
debian-7.11.0-source-CD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-06 (Wheezy) source CDs"
	URL=$url_debian_7_11_0/$VERSION/source/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-source-CD-1.iso 'Source CD #1 | 643M' 0
	add_dialog_item debian-$VERSION-source-CD-2.iso 'Source CD #2 | 590M' 0
	add_dialog_item debian-$VERSION-source-CD-3.iso 'Source CD #3 | 625M' 0
	add_dialog_item debian-$VERSION-source-CD-4.iso 'Source CD #4 | 612M' 0
	add_dialog_item debian-$VERSION-source-CD-5.iso 'Source CD #5 | 644M' 0
	add_dialog_item debian-$VERSION-source-CD-6.iso 'Source CD #6 | 169M' 0
	;;
debian-7.11.0-source-DVD)
	VERSION=7.11.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2016-06-05 (Wheezy) source DVDs"
	URL=$url_debian_7_11_0/$VERSION/source/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-source-DVD-1.iso        'Source DVD #1        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-2.iso        'Source DVD #2        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-3.iso        'Source DVD #3        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-4.iso        'Source DVD #4        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-5.iso        'Source DVD #5        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-6.iso        'Source DVD #6        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-7.iso        'Source DVD #7        | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-8.iso        'Source DVD #8        | 2.2G' 0
	add_dialog_item debian-update-$VERSION-source-DVD-1.iso 'Update source DVD #1 | 3.1G' 0
	;;
