# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

debian-live-testing-weekly-amd64+nonfree-DVD)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux Live $VERSION (weekly builds) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_testing/weekly-live-builds/amd64/iso-hybrid
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
        add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso          'Cinnamon desktop live DVD' 0
        add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso.contents ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso.log      ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso.packages ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-cinnamon.log          ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-gnome.iso             'GNOME desktop live DVD   ' 0
        add_dialog_item debian-live-$VERSION-amd64-gnome.iso.contents    ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-gnome.iso.log         ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-gnome.iso.packages    ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-gnome.log             ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-kde.iso               'KDE desktop live DVD     ' 0
        add_dialog_item debian-live-$VERSION-amd64-kde.iso.contents      ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-kde.iso.log           ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-kde.iso.packages      ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-kde.log               ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxde.iso              'LXDE desktop live DVD    ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxde.iso.contents     ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxde.iso.log          ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxde.iso.packages     ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxde.log              ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxqt.iso              'LXQt desktop live DVD    ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxqt.iso.contents     ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxqt.iso.log          ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxqt.iso.packages     ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-lxqt.log              ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-mate.iso              'MATE desktop live DVD    ' 0
        add_dialog_item debian-live-$VERSION-amd64-mate.iso.contents     ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-mate.iso.log          ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-mate.iso.packages     ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-mate.log              ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-standard.iso          'Standard desktop live DVD' 0
        add_dialog_item debian-live-$VERSION-amd64-standard.iso.contents ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-standard.iso.log      ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-standard.iso.packages ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-standard.log          ' * log file              ' 0
        add_dialog_item debian-live-$VERSION-amd64-xfce.iso              'Xfce desktop live DVD    ' 0
        add_dialog_item debian-live-$VERSION-amd64-xfce.iso.contents     ' * iso.contents file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-xfce.iso.log          ' * iso.log file          ' 0
        add_dialog_item debian-live-$VERSION-amd64-xfce.iso.packages     ' * iso.packages file     ' 0
        add_dialog_item debian-live-$VERSION-amd64-xfce.log              ' * log file              ' 0
	;;
debian-live-testing-weekly-source)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux Live $VERSION (weekly builds) sources"
	URL=$url_debian_testing/weekly-live-builds/source/tar
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
        add_dialog_item debian-live-$VERSION-source-cinnamon.tar      'Cinnamon desktop live source .tar file' 0
        add_dialog_item debian-live-$VERSION-source-cinnamon.contents ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-gnome.tar         'GNOME desktop live source .tar file   ' 0
        add_dialog_item debian-live-$VERSION-source-gnome.contents    ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-kde.tar           'KDE desktop live source .tar file     ' 0
        add_dialog_item debian-live-$VERSION-source-kde.contents      ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-lxde.tar          'LXDE desktop live source .tar file    ' 0
        add_dialog_item debian-live-$VERSION-source-lxde.contents     ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-lxqt.tar          'LXQt desktop live source .tar file    ' 0
        add_dialog_item debian-live-$VERSION-source-lxqt.contents     ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-mate.tar          'MATE desktop live source .tar file    ' 0
        add_dialog_item debian-live-$VERSION-source-mate.contents     ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-standard.tar      'Standard desktop live source .tar file' 0
        add_dialog_item debian-live-$VERSION-source-standard.contents ' * .contents file                     ' 0
        add_dialog_item debian-live-$VERSION-source-xfce.tar          'Xfce desktop live source .tar file    ' 0
        add_dialog_item debian-live-$VERSION-source-xfce.contents     ' * .contents file                     ' 0
	;;
debian-testing-daily-amd64+nonfree-CD)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux $VERSION (daily builds) AMD64 CDs (+non-free firmware)"
	URL=$url_debian_testing/daily-builds/daily/current/amd64/iso-cd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-netinst.iso     'Network installation CD    ' 0
	add_dialog_item debian-edu-$VERSION-amd64-netinst.iso 'Edu Network installation CD' 0
	;;
debian-testing-daily-amd64+sid_d-i+nonfree-CD)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux $VERSION (daily builds) AMD64 CDs (+Sid Debian-Installer, +non-free firmware)"
	URL=$url_debian_testing/daily-builds/sid_d-i/current/amd64/iso-cd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-netinst.iso     'Network installation CD    ' 0
	add_dialog_item debian-edu-$VERSION-amd64-netinst.iso 'Edu Network installation CD' 0
	;;
debian-testing-weekly-amd64+nonfree-CD)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux $VERSION (weekly builds) AMD64 CDs (+non-free firmware)"
	URL=$url_debian_testing/weekly-builds/amd64/iso-cd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-netinst.iso     'Network installation CD    ' 0
	add_dialog_item debian-edu-$VERSION-amd64-netinst.iso 'Edu Network installation CD' 0
	add_dialog_item debian-mac-$VERSION-amd64-netinst.iso 'Mac Network installation CD' 0
	;;
debian-testing-weekly-amd64+nonfree-DVD)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux $VERSION (weekly builds) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_testing/weekly-builds/amd64/iso-dvd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-DVD-1.iso 'Installation DVD #1' 0
	;;
debian-testing-weekly-source-DVD)
	VERSION=testing
	DLTYPE="Official Debian GNU/Linux $VERSION (weekly builds) source DVDs"
	URL=$url_debian_testing/weekly-builds/source/iso-dvd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-source-DVD-1.iso  'Source DVD #1 ' 0
	add_dialog_item debian-$VERSION-source-DVD-2.iso  'Source DVD #2 ' 0
	add_dialog_item debian-$VERSION-source-DVD-3.iso  'Source DVD #3 ' 0
	add_dialog_item debian-$VERSION-source-DVD-4.iso  'Source DVD #4 ' 0
	add_dialog_item debian-$VERSION-source-DVD-5.iso  'Source DVD #5 ' 0
	add_dialog_item debian-$VERSION-source-DVD-6.iso  'Source DVD #6 ' 0
	add_dialog_item debian-$VERSION-source-DVD-7.iso  'Source DVD #7 ' 0
	add_dialog_item debian-$VERSION-source-DVD-8.iso  'Source DVD #8 ' 0
	add_dialog_item debian-$VERSION-source-DVD-9.iso  'Source DVD #9 ' 0
	add_dialog_item debian-$VERSION-source-DVD-10.iso 'Source DVD #10' 0
	add_dialog_item debian-$VERSION-source-DVD-11.iso 'Source DVD #11' 0
	add_dialog_item debian-$VERSION-source-DVD-12.iso 'Source DVD #12' 0
	add_dialog_item debian-$VERSION-source-DVD-13.iso 'Source DVD #13' 0
	add_dialog_item debian-$VERSION-source-DVD-14.iso 'Source DVD #14' 0
	add_dialog_item debian-$VERSION-source-DVD-15.iso 'Source DVD #15' 0
	add_dialog_item debian-$VERSION-source-DVD-16.iso 'Source DVD #16' 0
	add_dialog_item debian-$VERSION-source-DVD-17.iso 'Source DVD #17' 0
	add_dialog_item debian-$VERSION-source-DVD-18.iso 'Source DVD #18' 0
	add_dialog_item debian-$VERSION-source-DVD-19.iso 'Source DVD #19' 0
	add_dialog_item debian-$VERSION-source-DVD-20.iso 'Source DVD #20' 0
	;;
