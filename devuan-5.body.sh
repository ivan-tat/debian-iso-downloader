# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

devuan-5)
	DLTYPE="Devuan GNU+Linux 5.x.x / 2023-08-14 (Daedalus) Information"
	URL=$url_devuan_5
	add_dialog_item Release_notes.txt 'Release_notes.txt | 10K' 1
	;;
devuan-5.0.0-desktop-live)
	DLTYPE="Devuan GNU+Linux 5.0.0 / 2023-08-14 (Daedalus) Desktop Live DVDs"
	URL=$url_devuan_5/desktop-live
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README_desktop-live.txt                      'README_desktop-live.txt | 5349' 1
	add_dialog_item devuan_daedalus_5.0.0_amd64_desktop-live.iso 'AMD64 Desktop Live DVD  | 1.4G' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_desktop-live.iso  'i386 Desktop Live DVD   | 1.3G' 0
	;;
devuan-5.0.0-minimal-live)
	DLTYPE="Devuan GNU+Linux 5.0.0 / 2023-08-14 (Daedalus) Minimal Live CDs"
	URL=$url_devuan_5/minimal-live
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README_minimal-live.txt                      'README_minimal-live.txt | 6653' 1
	add_dialog_item devuan_daedalus_5.0.0_amd64_minimal-live.iso 'AMD64 Minimal Live CD   | 798M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_minimal-live.iso  'i386 Minimal Live CD    | 717M' 0
	;;
devuan-5.0.0-installer-iso)
	DLTYPE="Devuan GNU+Linux 5.0.0 / 2023-08-14 (Daedalus) Installer CDs & DVDs"
	URL=$url_devuan_5/installer-iso
	add_checksums_file SHA256 SHA256SUMS.txt; add_extra_file SHA256SUMS.txt.asc
	add_dialog_item README.txt                                 'README.txt                  | 4711' 1
	add_dialog_item devuan_daedalus_5.0.0_amd64_desktop.iso    'AMD64 Desktop installer DVD | 3.8G' 0
	add_dialog_item devuan_daedalus_5.0.0_amd64_server.iso     'AMD64 Server installer DVD  | 760M' 0
	add_dialog_item devuan_daedalus_5.0.0_amd64_netinstall.iso 'AMD64 Network installer CD  | 477M' 0
	add_dialog_item devuan_daedalus_5.0.0_amd64_pool1.iso      'AMD64 Pool DVD #1           | 5.9M' 0
	add_dialog_item devuan_daedalus_5.0.0_amd64_cd2.iso        'AMD64 Installer CD #2       | 574M' 0
	add_dialog_item devuan_daedalus_5.0.0_amd64_cd3.iso        'AMD64 Installer CD #3       | 587M' 0
	add_dialog_item devuan_daedalus_5.0.0_amd64_cd4.iso        'AMD64 Installer CD #4       | 588M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_desktop.iso     'i386 Desktop installer DVD  | 3.7G' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_server.iso      'i386 Server installer DVD   | 779M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_netinstall.iso  'i386 Network installer CD   | 483M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_pool1.iso       'i386 Pool DVD #1            | 5.1M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_cd2.iso         'i386 Installer CD #2        | 590M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_cd3.iso         'i386 Installer CD #3        | 600M' 0
	add_dialog_item devuan_daedalus_5.0.0_i386_cd4.iso         'i386 Installer CD #4        | 590M' 0
	;;
