# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_devuan_1_0_0=$url_devuan_release/devuan_jessie

add_dialog_item devuan-1.0.0               '1.0.0 / 2017-05-04 (Jessie) Information' 0
add_dialog_item devuan-1.0.0-desktop-live  '1.0.0 / 2017-05-04 (Jessie) Desktop Live DVDs' 0
add_dialog_item devuan-1.0.0-minimal-live  '1.0.0 / 2017-05-04 (Jessie) Minimal Live CDs' 0
add_dialog_item devuan-1.0.0-installer-iso '1.0.0 / 2017-05-04 (Jessie) Installer CDs & DVDs' 0
add_dialog_item devuan-1.0.0-virtual       '1.0.0 / 2017-05-04 (Jessie) Virtual images' 0
