# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_debian_8_11_0=$url_debian_archive
url_debian_8_11_0_fw=$url_debian_fw_archive
url_debian_8_11_1=$url_debian_archive
url_debian_8_11_1_fw=$url_debian_fw_archive

add_dialog_item debian-live-8.11.0-amd64-DVD         'Live 8.11.0 / 2018-06-23 (Jessie) AMD64 DVDs' 0
add_dialog_item debian-live-8.11.0-amd64+nonfree-DVD 'Live 8.11.0 / 2018-06-23 (Jessie) AMD64 DVDs (+non-free firmware)' 0
add_dialog_item debian-live-8.11.0-i386-DVD          'Live 8.11.0 / 2018-06-23 (Jessie) i386  DVDs' 0
add_dialog_item debian-live-8.11.0-i386+nonfree-DVD  'Live 8.11.0 / 2018-06-23 (Jessie) i386  DVDs (+non-free firmware)' 0
add_dialog_item debian-live-8.11.0-source            'Live 8.11.0 / 2018-06-23 (Jessie) sources' 0
add_dialog_item debian-live-8.11.0-source+nonfree    'Live 8.11.0 / 2018-06-23 (Jessie) sources (+non-free firmware)' 0
add_dialog_item debian-8.11.1-amd64-CD               '     8.11.1 / 2019-02-11 (Jessie) AMD64 CDs' 0
add_dialog_item debian-8.11.1-amd64+nonfree-CD       '     8.11.1 / 2019-02-11 (Jessie) AMD64 CDs  (+non-free firmware)' 0
add_dialog_item debian-8.11.1-i386-CD                '     8.11.1 / 2019-02-11 (Jessie) i386  CDs' 0
add_dialog_item debian-8.11.1-i386+nonfree-CD        '     8.11.1 / 2019-02-11 (Jessie) i386  CDs  (+non-free firmware)' 0
add_dialog_item debian-8.11.1-amd64-DVD              '     8.11.1 / 2019-02-11 (Jessie) AMD64 DVDs' 0
add_dialog_item debian-8.11.1-i386-DVD               '     8.11.1 / 2019-02-11 (Jessie) i386  DVDs' 0
add_dialog_item debian-8.11.1-amd64-i386-CD          '     8.11.1 / 2019-02-11 (Jessie) AMD64+i386 CDs' 0
add_dialog_item debian-8.11.1-amd64-i386+nonfree-CD  '     8.11.1 / 2019-02-11 (Jessie) AMD64+i386 CDs (+non-free firmware)' 0
add_dialog_item debian-8.11.1-source-DVD             '     8.11.1 / 2019-02-11 (Jessie) source DVDs' 0
