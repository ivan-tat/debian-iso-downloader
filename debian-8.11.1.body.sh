# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

debian-live-8.11.0-amd64-DVD)
	VERSION=8.11.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2018-06-23 (Jessie) AMD64 DVDs"
	URL=$url_debian_8_11_0/$VERSION-live/amd64/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop.iso          'Cinnamon desktop live DVD | 1.1G' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop.iso.contents ' * .iso.contents file     |  22K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop.iso.log      ' * .iso.log file          | 1.3M' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop.iso.packages ' * .iso.packages file     |  47K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop.iso.zsync    ' * .iso.zsync file        | 4.4M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso          	 'GNOME desktop live DVD    | 1.3G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.contents 	 ' * .iso.contents file     |  22K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.log      	 ' * .iso.log file          | 1.3M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.packages	 ' * .iso.packages file     |  51K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop.iso.zsync   	 ' * .iso.zsync file        | 5.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso               'KDE desktop live DVD      | 1.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.contents      ' * .iso.contents file     |  22K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.log           ' * .iso.log file          | 1.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.packages      ' * .iso.packages file     |  47K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop.iso.zsync         ' * .iso.zsync file        | 4.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso              'LXDE desktop live DVD     | 953M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.contents     ' * .iso.contents file     |  22K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.log          ' * .iso.log file          | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.packages     ' * .iso.packages file     |  37K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop.iso.zsync        ' * .iso.zsync file        | 3.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop.iso              'MATE desktop live DVD     | 1.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop.iso.contents     ' * .iso.contents file     |  21K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop.iso.log          ' * .iso.log file          | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop.iso.packages     ' * .iso.packages file     |  36K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop.iso.zsync        ' * .iso.zsync file        | 3.9M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso                  'Standard desktop live DVD | 422M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.contents         ' * .iso.contents file     |  22K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.log              ' * .iso.log file          | 535K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.packages         ' * .iso.packages file     |  14K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.zsync            ' * .iso.zsync file        | 1.4M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso              'Xfce desktop live DVD     | 947M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.contents     ' * .iso.contents file     |  22K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.log          ' * .iso.log file          | 1.1M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.packages     ' * .iso.packages file     |  40K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop.iso.zsync        ' * .iso.zsync file        | 3.7M' 0
	;;
debian-live-8.11.0-amd64+nonfree-DVD)
	VERSION=8.11.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2018-06-23 (Jessie) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_8_11_0_fw/$VERSION-live+nonfree/amd64/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop+nonfree.iso          'Cinnamon desktop live DVD | 1.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop+nonfree.iso.contents ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop+nonfree.iso.log      ' * .iso.log file          | 1.4M' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop+nonfree.iso.packages ' * .iso.packages file     |  49K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon-desktop+nonfree.iso.zsync    ' * .iso.zsync file        | 5.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso          	 'GNOME desktop live DVD    | 1.4G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.contents 	 ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.log      	 ' * .iso.log file          | 1.5M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.packages	 ' * .iso.packages file     |  53K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome-desktop+nonfree.iso.zsync   	 ' * .iso.zsync file        | 5.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso               'KDE desktop live DVD      | 1.3G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.contents      ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.log           ' * .iso.log file          | 1.4M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.packages      ' * .iso.packages file     |  49K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde-desktop+nonfree.iso.zsync         ' * .iso.zsync file        | 5.3M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso              'LXDE desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.contents     ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.log          ' * .iso.log file          | 1.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.packages     ' * .iso.packages file     |  38K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde-desktop+nonfree.iso.zsync        ' * .iso.zsync file        | 4.3M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop+nonfree.iso              'MATE desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop+nonfree.iso.contents     ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop+nonfree.iso.log          ' * .iso.log file          | 1.1M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop+nonfree.iso.packages     ' * .iso.packages file     |  38K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate-desktop+nonfree.iso.zsync        ' * .iso.zsync file        | 4.4M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso                  'Standard desktop live DVD | 479M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.contents         ' * .iso.contents file     |  26K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.log              ' * .iso.log file          | 626K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.packages         ' * .iso.packages file     |  15K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso.zsync            ' * .iso.zsync file        | 1.6M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso              'Xfce desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.contents     ' * .iso.contents file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.log          ' * .iso.log file          | 1.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.packages     ' * .iso.packages file     |  42K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce-desktop+nonfree.iso.zsync        ' * .iso.zsync file        | 4.3M' 0
	;;
debian-live-8.11.0-i386-DVD)
	VERSION=8.11.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2018-06-23 (Jessie) i386 DVDs"
	URL=$url_debian_8_11_0/$VERSION-live/i386/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop.iso          'Cinnamon desktop live DVD | 1.2G' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop.iso.contents ' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop.iso.log      ' * .iso.log file          | 2.3M' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop.iso.packages ' * .iso.packages file     |  47K' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop.iso.zsync    ' * .iso.zsync file        | 4.9M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso          	'GNOME desktop live DVD    | 1.4G' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.contents 	' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.log      	' * .iso.log file          | 2.4M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.packages	' * .iso.packages file     |  50K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop.iso.zsync   	' * .iso.zsync file        | 5.7M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso               'KDE desktop live DVD      | 1.3G' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.contents      ' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.log           ' * .iso.log file          | 2.2M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.packages      ' * .iso.packages file     |  47K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop.iso.zsync         ' * .iso.zsync file        | 5.3M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso              'LXDE desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.contents     ' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.log          ' * .iso.log file          | 1.9M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.packages     ' * .iso.packages file     |  36K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop.iso.zsync        ' * .iso.zsync file        | 4.3M' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop.iso              'MATE desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop.iso.contents     ' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop.iso.log          ' * .iso.log file          | 1.8M' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop.iso.packages     ' * .iso.packages file     |  36K' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop.iso.zsync        ' * .iso.zsync file        | 4.4M' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso                  'Standard desktop live DVD | 530M' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.contents         ' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.log              ' * .iso.log file          | 884K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.packages         ' * .iso.packages file     |  14K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso.zsync            ' * .iso.zsync file        | 1.8M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso              'Xfce desktop live DVD     | 1.1G' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.contents     ' * .iso.contents file     |  24K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.log          ' * .iso.log file          | 1.9M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.packages     ' * .iso.packages file     |  39K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop.iso.zsync        ' * .iso.zsync file        | 4.2M' 0
	;;
debian-live-8.11.0-i386+nonfree-DVD)
	VERSION=8.11.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2018-06-23 (Jessie) i386 DVDs (+non-free firmware)"
	URL=$url_debian_8_11_0_fw/$VERSION-live+nonfree/i386/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop+nonfree.iso          'Cinnamon desktop live DVD | 1.4G' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop+nonfree.iso.contents ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop+nonfree.iso.log      ' * .iso.log file          | 2.4M' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop+nonfree.iso.packages ' * .iso.packages file     |  49K' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon-desktop+nonfree.iso.zsync    ' * .iso.zsync file        | 5.6M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso             'GNOME desktop live DVD    | 1.6G' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.contents    ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.log         ' * .iso.log file          | 2.6M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.packages    ' * .iso.packages file     |  52K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome-desktop+nonfree.iso.zsync       ' * .iso.zsync file        | 6.4M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso               'KDE desktop live DVD      | 1.5G' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.contents      ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.log           ' * .iso.log file          | 2.4M' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.packages      ' * .iso.packages file     |  49K' 0
	add_dialog_item debian-live-$VERSION-i386-kde-desktop+nonfree.iso.zsync         ' * .iso.zsync file        | 6.0M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso              'LXDE desktop live DVD     | 1.2G' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.contents     ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.log          ' * .iso.log file          | 2.0M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.packages     ' * .iso.packages file     |  38K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde-desktop+nonfree.iso.zsync        ' * .iso.zsync file        | 4.9M' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop+nonfree.iso              'MATE desktop live DVD     | 1.3G' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop+nonfree.iso.contents     ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop+nonfree.iso.log          ' * .iso.log file          | 2.0M' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop+nonfree.iso.packages     ' * .iso.packages file     |  37K' 0
	add_dialog_item debian-live-$VERSION-i386-mate-desktop+nonfree.iso.zsync        ' * .iso.zsync file        | 5.1M' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso                  'Standard desktop live DVD | 598M' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.contents         ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.log              ' * .iso.log file          | 1.0M' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.packages         ' * .iso.packages file     |  15K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso.zsync            ' * .iso.zsync file        | 2.0M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso              'Xfce desktop live DVD     | 1.2G' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.contents     ' * .iso.contents file     |  28K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.log          ' * .iso.log file          | 2.1M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.packages     ' * .iso.packages file     |  41K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce-desktop+nonfree.iso.zsync        ' * .iso.zsync file        | 4.9M' 0
	;;
debian-live-8.11.0-source)
	VERSION=8.11.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2018-06-23 (Jessie) sources"
	URL=$url_debian_8_11_0/$VERSION-live/source/tar
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-cinnamon-desktop.debian.tar      'Cinnamon desktop live source .tar file | 3.3G' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon-desktop.debian.contents ' * .contents file                      | 155K' 0
	add_dialog_item debian-live-$VERSION-source-gnome-desktop.debian.tar         'GNOME desktop live source .tar file    | 3.3G' 0
	add_dialog_item debian-live-$VERSION-source-gnome-desktop.debian.contents    ' * .contents file                      | 159K' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop.debian.tar           'KDE desktop live source .tar file      | 3.4G' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop.debian.contents      ' * .contents file                      | 138K' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop.debian.tar          'LXDE desktop live source .tar file     | 2.8G' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop.debian.contents     ' * .contents file                      | 129K' 0
	add_dialog_item debian-live-$VERSION-source-mate-desktop.debian.tar          'MATE desktop live source .tar file     | 2.8G' 0
	add_dialog_item debian-live-$VERSION-source-mate-desktop.debian.contents     ' * .contents file                      | 123K' 0
	add_dialog_item debian-live-$VERSION-source-standard.debian.tar              'Standard desktop live source .tar file | 1.0G' 0
	add_dialog_item debian-live-$VERSION-source-standard.debian.contents         ' * .contents file                      |  54K' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop.debian.tar          'Xfce desktop live source .tar file     | 2.8G' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop.debian.contents     ' * .contents file                      | 135K' 0
	;;
debian-live-8.11.0-source+nonfree)
	VERSION=8.11.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2018-06-23 (Jessie) sources (+non-free firmware)"
	URL=$url_debian_8_11_0_fw/$VERSION-live+nonfree/source/tar
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-cinnamon-desktop+nonfree.debian.tar      'Cinnamon desktop live source .tar file | 3.6G' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon-desktop+nonfree.debian.contents ' * .contents file                      | 159K' 0
	add_dialog_item debian-live-$VERSION-source-gnome-desktop+nonfree.debian.tar         'GNOME desktop live source .tar file    | 3.7G' 0
	add_dialog_item debian-live-$VERSION-source-gnome-desktop+nonfree.debian.contents    ' * .contents file                      | 163K' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop+nonfree.debian.tar           'KDE desktop live source .tar file      | 3.7G' 0
	add_dialog_item debian-live-$VERSION-source-kde-desktop+nonfree.debian.contents      ' * .contents file                      | 142K' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop+nonfree.debian.tar          'LXDE desktop live source .tar file     | 3.1G' 0
	add_dialog_item debian-live-$VERSION-source-lxde-desktop+nonfree.debian.contents     ' * .contents file                      | 133K' 0
	add_dialog_item debian-live-$VERSION-source-mate-desktop+nonfree.debian.tar          'MATE desktop live source .tar file     | 3.1G' 0
	add_dialog_item debian-live-$VERSION-source-mate-desktop+nonfree.debian.contents     ' * .contents file                      | 127K' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.debian.tar              'Standard desktop live source .tar file | 1.0G' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.debian.contents         ' * .contents file                      |  56K' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop+nonfree.debian.tar          'Xfce desktop live source .tar file     | 3.1G' 0
	add_dialog_item debian-live-$VERSION-source-xfce-desktop+nonfree.debian.contents     ' * .contents file                      | 139K' 0
	;;
debian-8.11.1-amd64-CD)
	VERSION=8.11.1
	DLTYPE="Official Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) AMD64 CDs"
	URL=$url_debian_8_11_1/$VERSION/amd64/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-CD-1.iso        'Installation CD #1              | 628M' 0
	add_dialog_item debian-$VERSION-amd64-CD-2.iso        'Installation CD #2              | 643M' 0
	add_dialog_item debian-$VERSION-amd64-CD-3.iso        'Installation CD #3              | 645M' 0
	add_dialog_item debian-$VERSION-amd64-CD-4.iso        'Installation CD #4              | 573M' 0
	add_dialog_item debian-$VERSION-amd64-CD-5.iso        'Installation CD #5              | 645M' 0
	add_dialog_item debian-$VERSION-amd64-CD-6.iso        'Installation CD #6              | 646M' 0
	add_dialog_item debian-$VERSION-amd64-CD-7.iso        'Installation CD #7              | 646M' 0
	add_dialog_item debian-$VERSION-amd64-CD-8.iso        'Installation CD #8              | 645M' 0
	add_dialog_item debian-$VERSION-amd64-kde-CD-1.iso    'KDE desktop installation CD #1  | 629M' 0
	add_dialog_item debian-$VERSION-amd64-lxde-CD-1.iso   'LXDE desktop installation CD #1 | 641M' 0
	add_dialog_item debian-$VERSION-amd64-netinst.iso     'Network installation CD         | 251M' 0
	add_dialog_item debian-$VERSION-amd64-xfce-CD-1.iso   'Xfce desktop installation CD #1 | 598M' 0
	add_dialog_item debian-mac-$VERSION-amd64-netinst.iso 'Mac network installation CD     | 252M' 0
	;;
debian-8.11.1-amd64+nonfree-CD)
	VERSION=8.11.1
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) AMD64 CDs (+non-free firmware)"
	URL=$url_debian_8_11_1_fw/$VERSION+nonfree/amd64/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-netinst.iso 'Network installation CD | 270M' 0
	;;
debian-8.11.1-i386-CD)
	VERSION=8.11.1
	DLTYPE="Official Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) i386 CDs"
	URL=$url_debian_8_11_1/$VERSION/i386/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-CD-1.iso        'Installation CD #1              | 639M' 0
	add_dialog_item debian-$VERSION-i386-CD-2.iso        'Installation CD #2              | 637M' 0
	add_dialog_item debian-$VERSION-i386-CD-3.iso        'Installation CD #3              | 604M' 0
	add_dialog_item debian-$VERSION-i386-CD-4.iso        'Installation CD #4              | 645M' 0
	add_dialog_item debian-$VERSION-i386-CD-5.iso        'Installation CD #5              | 633M' 0
	add_dialog_item debian-$VERSION-i386-CD-6.iso        'Installation CD #6              | 639M' 0
	add_dialog_item debian-$VERSION-i386-CD-7.iso        'Installation CD #7              | 646M' 0
	add_dialog_item debian-$VERSION-i386-CD-8.iso        'Installation CD #8              | 634M' 0
	add_dialog_item debian-$VERSION-i386-kde-CD-1.iso    'KDE desktop installation CD #1  | 634M' 0
	add_dialog_item debian-$VERSION-i386-lxde-CD-1.iso   'LXDE desktop installation CD #1 | 613M' 0
	add_dialog_item debian-$VERSION-i386-netinst.iso     'Network installation CD         | 321M' 0
	add_dialog_item debian-$VERSION-i386-xfce-CD-1.iso   'Xfce desktop installation CD #1 | 640M' 0
	add_dialog_item debian-mac-$VERSION-i386-netinst.iso 'Mac network installation CD     | 323M' 0
	;;
debian-8.11.1-i386+nonfree-CD)
	VERSION=8.11.1
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) i386 CDs (+non-free firmware)"
	URL=$url_debian_8_11_1_fw/$VERSION+nonfree/i386/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-i386-netinst.iso 'Network installation CD | 341M' 0
	;;
debian-8.11.1-amd64-DVD)
	VERSION=8.11.1
	DLTYPE="Official Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) AMD64 DVDs"
	URL=$url_debian_8_11_1/$VERSION/amd64/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-DVD-1.iso 'Installation DVD #1 | 3.7G' 0
	add_dialog_item debian-$VERSION-amd64-DVD-2.iso 'Installation DVD #2 | 4.3G' 0
	add_dialog_item debian-$VERSION-amd64-DVD-3.iso 'Installation DVD #3 | 4.3G' 0
	;;
debian-8.11.1-i386-DVD)
	VERSION=8.11.1
	DLTYPE="Official Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) i386 DVDs"
	URL=$url_debian_8_11_1/$VERSION/i386/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-DVD-1.iso 'Installation DVD #1 | 3.6G' 0
	add_dialog_item debian-$VERSION-i386-DVD-2.iso 'Installation DVD #2 | 4.4G' 0
	add_dialog_item debian-$VERSION-i386-DVD-3.iso 'Installation DVD #3 | 4.3G' 0
	;;
debian-8.11.1-amd64-i386-CD)
	VERSION=8.11.1
	DLTYPE="Official Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) AMD64+i386 CDs"
	URL=$url_debian_8_11_1/$VERSION/multi-arch/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-i386-netinst.iso 'Network installation CD | 565M' 0
	;;
debian-8.11.1-amd64-i386+nonfree-CD)
	VERSION=8.11.1
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) AMD64+i386 CDs (+non-free firmware)"
	URL=$url_debian_8_11_1_fw/$VERSION+nonfree/multi-arch/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-i386-netinst.iso 'Network installation CD | 586M' 0
	;;
debian-8.11.1-source-DVD)
	VERSION=8.11.1
	DLTYPE="Official Debian GNU/Linux $VERSION / 2019-02-11 (Jessie) source DVDs"
	URL=$url_debian_8_11_1/$VERSION/source/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-source-DVD-1.iso  'Source DVD #1  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-2.iso  'Source DVD #2  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-3.iso  'Source DVD #3  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-4.iso  'Source DVD #4  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-5.iso  'Source DVD #5  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-6.iso  'Source DVD #6  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-7.iso  'Source DVD #7  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-8.iso  'Source DVD #8  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-9.iso  'Source DVD #9  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-10.iso 'Source DVD #10 | 3.5G' 0
	;;
