#!/bin/bash -e
# Multiple HTML downloader and parser
# Version 1.0
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2024 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

if [ $# -ne 1 ]; then
	cat <<-EOT
	Multiple HTML downloader and parser

	Usage:
	  $0 VERSION
	where:
	  VERSION is in format A.B.C as shown on Debian's web-site.
	EOT
	exit 1
fi

VERSION=$1
BASE_URL=https://cdimage.debian.org/cdimage
REMOTE_DIRS="\
 release/${VERSION}-live/amd64/iso-hybrid\
 release/${VERSION}-live/source/tar\
 release/${VERSION}/amd64/iso-cd\
 release/${VERSION}/i386/iso-cd\
 release/${VERSION}/amd64/iso-dvd\
 release/${VERSION}/i386/iso-dvd\
 release/${VERSION}/source/iso-dvd"

i=1
for way in $REMOTE_DIRS; do
	wget $BASE_URL/$way/ -O $(printf "%02d" $i)_${way//\//_}.html
	let i=i+1
done

i=1
for way in $REMOTE_DIRS; do
	f=$(printf "%02d" $i)_${way//\//_}.html
	./parse-index-html.sh $f > ${f%.html}.txt
	let i=i+1
done
