#!/bin/sh -e
# HTML parser
# Version 1.0
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

if [ $# -ne 1 ]; then
	cat <<-EOT
	HTML parser

	Usage:
	  $0 FILE
	where:
	  FILE is an "index.html" file of FTP folder saved from Debian's web-site.

	An output is a three columns: filename, modification time, file size.

	Example:
	  wget https://cdimage.debian.org/cdimage/archive/8.11.0-live/amd64/iso-hybrid/
	  $0 index.html > index.txt
	EOT
	exit 1
fi

grep -E '^ *<tr class="(even|odd)"><td class="indexcolicon"><a href="[^"]+"><img src="[^"]+" alt="[^"]+"></a></td><td class="indexcolname"><a href="[^"]+">[^>]+</a></td><td class="indexcollastmod">[^<]+ +</td><td class="indexcolsize">[^<]+</td></tr>' "$1" |\
sed -Ee 's, *<tr class="(even|odd)"><td class="indexcolicon"><a href="[^"]+"><img src="[^"]+" alt="[^"]+"></a></td><td class="indexcolname"><a href="[^"]+">([^>]+)</a></td><td class="indexcollastmod">([^<]+)</td><td class="indexcolsize">([^<]+)</td></tr>.*,\2\t| \3| \4,' |\
expand -t 100
