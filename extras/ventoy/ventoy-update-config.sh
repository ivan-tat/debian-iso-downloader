#!/bin/bash -e
# Ventoy configuration updater
# Version 1.0
# Put this script into "/ventoy" directory of your Ventoy boot partition.
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

declare menu_alias
OnExit() {
	if [ -n "$menu_alias" ]; then rm -f "$menu_alias"; fi
}
trap OnExit EXIT
menu_alias=`mktemp`
find . -name *.alias -type f | sort > "$menu_alias"
count=`cat "$menu_alias" | wc -l`
echo "Found $count menu aliases"
> ventoy_menu_alias.json
if [ "$count" -ne 0 ]; then
	while read f; do
		entry="${f:2:-6}"
		if [ -d "$entry" ]; then type='dir'; else type='image'; fi
		count=$((count-1))
		if [ $count -ne 0 ]; then s=','; else s=''; fi
		cat >> ventoy_menu_alias.json <<-EOT
		        {
		            "$type": "/ventoy/$entry",
		            "alias": "`cat "$f"`"
		        }$s
		EOT
	done < "$menu_alias"
	unset f
	unset entry
	unset s
	unset type
fi
unset count
rm -f "$menu_alias"
unset menu_alias
{
	echo '{'
	echo '    "control": ['
	cat ventoy_control.json
	echo '    ],'
	echo '    "menu_alias": ['
	cat ventoy_menu_alias.json
	echo '    ]'
	echo '}'
} > ventoy.json
echo 'Done.'
