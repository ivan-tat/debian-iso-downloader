#!/bin/bash -e
# Debian ISO renamer
# Version 1.0
# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

if [ $# -ne 1 ]; then
	cat <<-EOT
	Debian ISO renamer

	Usage:
	  $0 FILE
	where:
	  FILE is a filename of the following type:
	    firmware-#.#.#-<...>.iso   -> debian-#.#.#-<...>+nonfree.iso
	    firmware-testing-<...>.iso -> debian-testing-<...>+nonfree.DATE.iso
	  here: # is a sequense of digits 0-9, DATE is a file's date

	A new link will be created and if there're files FILE.sha256 and/or
	FILE.sha512 new files will be created with the same SHA sums.
	EOT
	exit 1
fi
if [[ "$1" =~ firmware-[0-9]+\.[0-9]+\.[0-9]+-.+\.iso$ ]]; then
	f="${1/firmware/debian}"; f="${f:0:-4}"+nonfree.iso
	ln -f "$1" "$f"
	if [ -f "$1.sha256" ]; then s=`grep "$1" "$1.sha256" | cut -d\  -f 1`; echo "$s  $f" >"$f.sha256"; fi
	if [ -f "$1.sha512" ]; then s=`grep "$1" "$1.sha512" | cut -d\  -f 1`; echo "$s  $f" >"$f.sha512"; fi
	echo "Created link $1 -> $f"
elif [[ "$1" =~ firmware-testing-.+\.iso$ ]]; then
	d=`stat -c %y "$1"`; d=${d:0:10}
	if [[ ! "$d"~=[0-9]{4}-[0-9]{2}-[0-9]{2} ]]; then echo "Failed to obtain file's date"; exit 1; fi
	f="${1/firmware/debian}"; f="${f:0:-4}"+nonfree.$d.iso
	ln -f "$1" "$f"
	if [ -f "$1.sha256" ]; then s=`grep "$1" "$1.sha256" | cut -d\  -f 1`; echo "$s  $f" >"$f.sha256"; fi
	if [ -f "$1.sha512" ]; then s=`grep "$1" "$1.sha512" | cut -d\  -f 1`; echo "$s  $f" >"$f.sha512"; fi
	echo "Created link $1 -> $f"
else
	echo 'Bad filename. See usage.'
	exit 1
fi
