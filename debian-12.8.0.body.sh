# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023-2024 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

debian-live-12.8.0-amd64+nonfree-DVD)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2024-11-09 (Bookworm) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_12_8_0/$VERSION-live/amd64/iso-hybrid
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso          'Cinnamon desktop live DVD | 3.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso.contents ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso.log      ' * .iso.log file          | 2.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso.packages ' * .iso.packages file     |  77K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.log          ' * .log file              | 2.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.iso             'GNOME desktop live DVD    | 3.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.iso.contents    ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.iso.log         ' * .iso.log file          | 1.9M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.iso.packages    ' * .iso.packages file     |  73K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.log             ' * .log file              | 2.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.iso               'KDE desktop live DVD      | 3.3G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.iso.contents      ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.iso.log           ' * .iso.log file          | 2.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.iso.packages      ' * .iso.packages file     |  81K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.log               ' * .log file              | 2.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.iso              'LXDE desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.iso.contents     ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.iso.log          ' * .iso.log file          | 1.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.iso.packages     ' * .iso.packages file     |  66K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.log              ' * .log file              | 1.9M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.iso              'LXQt desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.iso.contents     ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.iso.log          ' * .iso.log file          | 1.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.iso.packages     ' * .iso.packages file     |  65K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.log              ' * .log file              | 1.9M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.iso              'MATE desktop live DVD     | 3.1G' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.iso.contents     ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.iso.log          ' * .iso.log file          | 1.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.iso.packages     ' * .iso.packages file     |  65K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.log              ' * .log file              | 1.8M' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso          'Standard desktop live DVD | 1.4G' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.contents ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.log      ' * .iso.log file          | 811K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso.packages ' * .iso.packages file     |  25K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.iso              'Xfce desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.iso.contents     ' * .iso.contents file     |  52K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.iso.log          ' * .iso.log file          | 1.7M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.iso.packages     ' * .iso.packages file     |  65K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.log              ' * .log file              | 1.8M' 0
	;;
debian-live-12.8.0-source+nonfree)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2024-11-09 (Bookworm) sources (+non-free firmware)"
	URL=$url_debian_12_8_0/$VERSION-live/source/tar
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-cinnamon.tar      'Cinnamon desktop live source .tar file | 6.5G' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon.contents ' * .contents file                      | 236K' 0
	add_dialog_item debian-live-$VERSION-source-gnome.tar         'GNOME desktop live source .tar file    | 6.9G' 0
	add_dialog_item debian-live-$VERSION-source-gnome.contents    ' * .contents file                      | 205K' 0
	add_dialog_item debian-live-$VERSION-source-kde.tar           'KDE desktop live source .tar file      | 6.5G' 0
	add_dialog_item debian-live-$VERSION-source-kde.contents      ' * .contents file                      | 222K' 0
	add_dialog_item debian-live-$VERSION-source-lxde.tar          'LXDE desktop live source .tar file     | 6.2G' 0
	add_dialog_item debian-live-$VERSION-source-lxde.contents     ' * .contents file                      | 192K' 0
	add_dialog_item debian-live-$VERSION-source-lxqt.tar          'LXQt desktop live source .tar file     | 6.3G' 0
	add_dialog_item debian-live-$VERSION-source-lxqt.contents     ' * .contents file                      | 187K' 0
	add_dialog_item debian-live-$VERSION-source-mate.tar          'MATE desktop live source .tar file     | 6.2G' 0
	add_dialog_item debian-live-$VERSION-source-mate.contents     ' * .contents file                      | 183K' 0
	add_dialog_item debian-live-$VERSION-source-standard.tar      'Standard desktop live source .tar file | 2.7G' 0
	add_dialog_item debian-live-$VERSION-source-standard.contents ' * .contents file                      |  82K' 0
	add_dialog_item debian-live-$VERSION-source-xfce.tar          'Xfce desktop live source .tar file     | 6.1G' 0
	add_dialog_item debian-live-$VERSION-source-xfce.contents     ' * .contents file                      | 187K' 0
	;;
debian-12.8.0-amd64+nonfree-CD)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2024-11-09 (Bookworm) AMD64 CDs (+non-free firmware)"
	URL=$url_debian_12_8_0/$VERSION/amd64/iso-cd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-netinst.iso     'Network installation CD     | 631M' 0
	add_dialog_item debian-edu-$VERSION-amd64-netinst.iso 'Edu network installation CD | 639M' 0
	add_dialog_item debian-mac-$VERSION-amd64-netinst.iso 'Mac network installation CD | 629M' 0
	;;
debian-12.8.0-i386+nonfree-CD)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2024-11-09 (Bookworm) i386 CDs (+non-free firmware)"
	URL=$url_debian_12_8_0/$VERSION/i386/iso-cd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-netinst.iso     'Network installation CD     | 645M' 0
	add_dialog_item debian-edu-$VERSION-i386-netinst.iso 'Edu network installation CD | 653M' 0
	add_dialog_item debian-mac-$VERSION-i386-netinst.iso 'Mac network installation CD | 645M' 0
	;;
debian-12.8.0-amd64+nonfree-DVD)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2024-11-09 (Bookworm) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_12_8_0/$VERSION/amd64/iso-dvd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-DVD-1.iso 'Installation DVD #1 | 3.7G' 0
	;;
debian-12.8.0-i386+nonfree-DVD)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2024-11-09 (Bookworm) i386 DVDs (+non-free firmware)"
	URL=$url_debian_12_8_0/$VERSION/i386/iso-dvd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-DVD-1.iso 'Installation DVD #1 | 3.7G' 0
	;;
debian-12.8.0-source-DVD)
	VERSION=12.8.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2024-11-09 (Bookworm) source DVDs"
	URL=$url_debian_12_8_0/$VERSION/source/iso-dvd
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-source-DVD-1.iso  'Source DVD #1  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-2.iso  'Source DVD #2  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-3.iso  'Source DVD #3  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-4.iso  'Source DVD #4  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-5.iso  'Source DVD #5  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-6.iso  'Source DVD #6  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-7.iso  'Source DVD #7  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-8.iso  'Source DVD #8  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-9.iso  'Source DVD #9  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-10.iso 'Source DVD #10 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-11.iso 'Source DVD #11 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-12.iso 'Source DVD #12 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-13.iso 'Source DVD #13 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-14.iso 'Source DVD #14 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-15.iso 'Source DVD #15 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-16.iso 'Source DVD #16 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-17.iso 'Source DVD #17 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-18.iso 'Source DVD #18 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-19.iso 'Source DVD #19 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-19.iso 'Source DVD #20 | 2.8G' 0
	;;
