# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2022, 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

debian-live-10.13.0-amd64-DVD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2022-09-10 (Buster) AMD64 DVDs"
	URL=$url_debian_10_13_0/$VERSION-live/amd64/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.iso      'Cinnamon desktop live DVD | 2.5G' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.contents ' * .contents file         |  79K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.log      ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon.packages ' * .packages file         |  71K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.iso         'GNOME desktop live DVD    | 2.6G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.contents    ' * .contents file         |  79K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.log         ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome.packages    ' * .packages file         |  69K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.iso           'KDE desktop live DVD      | 2.7G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.contents      ' * .contents file         |  79K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.log           ' * .log file              | 1.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde.packages      ' * .packages file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.iso          'LXDE desktop live DVD     | 2.4G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.contents     ' * .contents file         |  80K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde.packages     ' * .packages file         |  66K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.iso          'LXQt desktop live DVD     | 2.5G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.contents     ' * .contents file         |  80K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt.packages     ' * .packages file         |  65K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.iso          'MATE desktop live DVD     | 2.5G' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.contents     ' * .contents file         |  80K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.log          ' * .log file              | 962K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate.packages     ' * .packages file         |  64K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.iso      'Standard desktop live DVD | 1.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.contents ' * .contents file         |  81K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.log      ' * .log file              | 402K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard.packages ' * .packages file         |  20K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.iso          'Xfce desktop live DVD     | 2.4G' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.contents     ' * .contents file         |  80K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce.packages     ' * .packages file         |  65K' 0
	;;
debian-live-10.13.0-amd64+nonfree-DVD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2022-09-10 (Buster) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION-live+nonfree/amd64/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-amd64-cinnamon+nonfree.iso      'Cinnamon desktop live DVD | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon+nonfree.contents ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon+nonfree.log      ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-amd64-cinnamon+nonfree.packages ' * .packages file         |  74K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome+nonfree.iso         'GNOME desktop live DVD    | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome+nonfree.contents    ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome+nonfree.log         ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-amd64-gnome+nonfree.packages    ' * .packages file         |  73K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde+nonfree.iso           'KDE desktop live DVD      | 3.2G' 0
	add_dialog_item debian-live-$VERSION-amd64-kde+nonfree.contents      ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-kde+nonfree.log           ' * .log file              | 1.2M' 0
	add_dialog_item debian-live-$VERSION-amd64-kde+nonfree.packages      ' * .packages file         |  86K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde+nonfree.iso          'LXDE desktop live DVD     | 2.9G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde+nonfree.contents     ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde+nonfree.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxde+nonfree.packages     ' * .packages file         |  70K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt+nonfree.iso          'LXQt desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt+nonfree.contents     ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt+nonfree.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-lxqt+nonfree.packages     ' * .packages file         |  69K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate+nonfree.iso          'MATE desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-amd64-mate+nonfree.contents     ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-mate+nonfree.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-mate+nonfree.packages     ' * .packages file         |  68K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.iso      'Standard desktop live DVD | 1.5G' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.contents ' * .contents file         |  83K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.log      ' * .log file              | 503K' 0
	add_dialog_item debian-live-$VERSION-amd64-standard+nonfree.packages ' * .packages file         |  27K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce+nonfree.iso          'Xfce desktop live DVD     | 2.9G' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce+nonfree.contents     ' * .contents file         |  82K' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce+nonfree.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-amd64-xfce+nonfree.packages     ' * .packages file         |  69K' 0
	;;
debian-live-10.13.0-i386-DVD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2022-09-10 (Buster) i386 DVDs"
	URL=$url_debian_10_13_0/$VERSION-live/i386/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-i386-cinnamon.iso      'Cinnamon desktop live DVD | 2.7G' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon.contents ' * .contents file         | 117K' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon.log      ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon.packages ' * .packages file         |  70K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome.iso         'GNOME desktop live DVD    | 2.7G' 0
	add_dialog_item debian-live-$VERSION-i386-gnome.contents    ' * .contents file         | 117K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome.log         ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome.packages    ' * .packages file         |  68K' 0
	add_dialog_item debian-live-$VERSION-i386-kde.iso           'KDE desktop live DVD      | 2.9G' 0
	add_dialog_item debian-live-$VERSION-i386-kde.contents      ' * .contents file         | 117K' 0
	add_dialog_item debian-live-$VERSION-i386-kde.log           ' * .log file              | 1.2M' 0
	add_dialog_item debian-live-$VERSION-i386-kde.packages      ' * .packages file         |  81K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde.iso          'LXDE desktop live DVD     | 2.6G' 0
	add_dialog_item debian-live-$VERSION-i386-lxde.contents     ' * .contents file         | 117K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde.packages     ' * .packages file         |  65K' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt.iso          'LXQt desktop live DVD     | 2.7G' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt.contents     ' * .contents file         | 117K' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt.packages     ' * .packages file         |  64K' 0
	add_dialog_item debian-live-$VERSION-i386-mate.iso          'MATE desktop live DVD     | 2.6G' 0
	add_dialog_item debian-live-$VERSION-i386-mate.contents     ' * .contents file         | 118K' 0
	add_dialog_item debian-live-$VERSION-i386-mate.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-i386-mate.packages     ' * .packages file         |  63K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.iso      'Standard desktop live DVD | 1.1G' 0
	add_dialog_item debian-live-$VERSION-i386-standard.contents ' * .contents file         | 119K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.log      ' * .log file              | 472K' 0
	add_dialog_item debian-live-$VERSION-i386-standard.packages ' * .packages file         |  20K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce.iso          'Xfce desktop live DVD     | 2.5G' 0
	add_dialog_item debian-live-$VERSION-i386-xfce.contents     ' * .contents file         | 117K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce.log          ' * .log file              | 1.0M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce.packages     ' * .packages file         |  64K' 0
	;;
debian-live-10.13.0-i386+nonfree-DVD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2022-09-10 (Buster) i386 DVDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION-live+nonfree/i386/iso-hybrid
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-i386-cinnamon+nonfree.iso      'Cinnamon desktop live DVD | 3.2G' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon+nonfree.contents ' * .contents file         | 119K' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon+nonfree.log      ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-cinnamon+nonfree.packages ' * .packages file         |  73K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome+nonfree.iso         'GNOME desktop live DVD    | 3.2G' 0
	add_dialog_item debian-live-$VERSION-i386-gnome+nonfree.contents    ' * .contents file         | 119K' 0
	add_dialog_item debian-live-$VERSION-i386-gnome+nonfree.log         ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-gnome+nonfree.packages    ' * .packages file         |  72K' 0
	add_dialog_item debian-live-$VERSION-i386-kde+nonfree.iso           'KDE desktop live DVD      | 3.4G' 0
	add_dialog_item debian-live-$VERSION-i386-kde+nonfree.contents      ' * .contents file         | 119K' 0
	add_dialog_item debian-live-$VERSION-i386-kde+nonfree.log           ' * .log file              | 1.3M' 0
	add_dialog_item debian-live-$VERSION-i386-kde+nonfree.packages      ' * .packages file         |  84K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde+nonfree.iso          'LXDE desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-i386-lxde+nonfree.contents     ' * .contents file         | 120K' 0
	add_dialog_item debian-live-$VERSION-i386-lxde+nonfree.log          ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-lxde+nonfree.packages     ' * .packages file         |  69K' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt+nonfree.iso          'LXQt desktop live DVD     | 3.2G' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt+nonfree.contents     ' * .contents file         | 120K' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt+nonfree.log          ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-lxqt+nonfree.packages     ' * .packages file         |  68K' 0
	add_dialog_item debian-live-$VERSION-i386-mate+nonfree.iso          'MATE desktop live DVD     | 3.1G' 0
	add_dialog_item debian-live-$VERSION-i386-mate+nonfree.contents     ' * .contents file         | 120K' 0
	add_dialog_item debian-live-$VERSION-i386-mate+nonfree.log          ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-mate+nonfree.packages     ' * .packages file         |  67K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.iso      'Standard desktop live DVD | 1.7G' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.contents ' * .contents file         | 121K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.log      ' * .log file              | 573K' 0
	add_dialog_item debian-live-$VERSION-i386-standard+nonfree.packages ' * .packages file         |  27K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce+nonfree.iso          'Xfce desktop live DVD     | 3.0G' 0
	add_dialog_item debian-live-$VERSION-i386-xfce+nonfree.contents     ' * .contents file         | 120K' 0
	add_dialog_item debian-live-$VERSION-i386-xfce+nonfree.log          ' * .log file              | 1.1M' 0
	add_dialog_item debian-live-$VERSION-i386-xfce+nonfree.packages     ' * .packages file         |  68K' 0
	;;
debian-live-10.13.0-source)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux Live $VERSION / 2022-09-10 (Buster) sources"
	URL=$url_debian_10_13_0/$VERSION-live/source/tar
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-cinnamon.tar      'Cinnamon desktop live source .tar file | 5.3G' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon.contents ' * .contents file                      | 126K' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon.log      ' * .log file                           |  10M' 0
	add_dialog_item debian-live-$VERSION-source-gnome.tar         'GNOME desktop live source .tar file    | 5.2G' 0
	add_dialog_item debian-live-$VERSION-source-gnome.contents    ' * .contents file                      | 117K' 0
	add_dialog_item debian-live-$VERSION-source-gnome.log         ' * .log file                           |  10M' 0
	add_dialog_item debian-live-$VERSION-source-kde.tar           'KDE desktop live source .tar file      | 5.5G' 0
	add_dialog_item debian-live-$VERSION-source-kde.contents      ' * .contents file                      | 139K' 0
	add_dialog_item debian-live-$VERSION-source-kde.log           ' * .log file                           |  11M' 0
	add_dialog_item debian-live-$VERSION-source-lxde.tar          'LXDE desktop live source .tar file     | 4.7G' 0
	add_dialog_item debian-live-$VERSION-source-lxde.contents     ' * .contents file                      | 118K' 0
	add_dialog_item debian-live-$VERSION-source-lxde.log          ' * .log file                           | 9.5M' 0
	add_dialog_item debian-live-$VERSION-source-lxqt.tar          'LXQt desktop live source .tar file     | 5.6G' 0
	add_dialog_item debian-live-$VERSION-source-lxqt.contents     ' * .contents file                      | 118K' 0
	add_dialog_item debian-live-$VERSION-source-lxqt.log          ' * .log file                           |  11M' 0
	add_dialog_item debian-live-$VERSION-source-mate.tar          'MATE desktop live source .tar file     | 4.7G' 0
	add_dialog_item debian-live-$VERSION-source-mate.contents     ' * .contents file                      | 113K' 0
	add_dialog_item debian-live-$VERSION-source-mate.log          ' * .log file                           | 9.3M' 0
	add_dialog_item debian-live-$VERSION-source-standard.tar      'Standard desktop live source .tar file | 1.2G' 0
	add_dialog_item debian-live-$VERSION-source-standard.contents ' * .contents file                      |  39K' 0
	add_dialog_item debian-live-$VERSION-source-standard.log      ' * .log file                           | 2.6M' 0
	add_dialog_item debian-live-$VERSION-source-xfce.tar          'Xfce desktop live source .tar file     | 4.7G' 0
	add_dialog_item debian-live-$VERSION-source-xfce.contents     ' * .contents file                      | 118K' 0
	add_dialog_item debian-live-$VERSION-source-xfce.log          ' * .log file                           | 9.4M' 0
	;;
debian-live-10.13.0-source+nonfree)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux Live $VERSION / 2022-09-10 (Buster) sources (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION-live+nonfree/source/tar
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-live-$VERSION-source-cinnamon+nonfree.tar      'Cinnamon desktop live source .tar file | 5.6G' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon+nonfree.contents ' * .contents file                      | 131K' 0
	add_dialog_item debian-live-$VERSION-source-cinnamon+nonfree.log      ' * .log file                           |  11M' 0
	add_dialog_item debian-live-$VERSION-source-gnome+nonfree.tar         'GNOME desktop live source .tar file    | 5.5G' 0
	add_dialog_item debian-live-$VERSION-source-gnome+nonfree.contents    ' * .contents file                      | 124K' 0
	add_dialog_item debian-live-$VERSION-source-gnome+nonfree.log         ' * .log file                           |  11M' 0
	add_dialog_item debian-live-$VERSION-source-kde+nonfree.tar           'KDE desktop live source .tar file      | 5.8G' 0
	add_dialog_item debian-live-$VERSION-source-kde+nonfree.contents      ' * .contents file                      | 145K' 0
	add_dialog_item debian-live-$VERSION-source-kde+nonfree.log           ' * .log file                           |  12M' 0
	add_dialog_item debian-live-$VERSION-source-lxde+nonfree.tar          'LXDE desktop live source .tar file     | 5.1G' 0
	add_dialog_item debian-live-$VERSION-source-lxde+nonfree.contents     ' * .contents file                      | 125K' 0
	add_dialog_item debian-live-$VERSION-source-lxde+nonfree.log          ' * .log file                           |  10M' 0
	add_dialog_item debian-live-$VERSION-source-lxqt+nonfree.tar          'LXQt desktop live source .tar file     | 5.9G' 0
	add_dialog_item debian-live-$VERSION-source-lxqt+nonfree.contents     ' * .contents file                      | 124K' 0
	add_dialog_item debian-live-$VERSION-source-lxqt+nonfree.log          ' * .log file                           |  11M' 0
	add_dialog_item debian-live-$VERSION-source-mate+nonfree.tar          'MATE desktop live source .tar file     | 5.0G' 0
	add_dialog_item debian-live-$VERSION-source-mate+nonfree.contents     ' * .contents file                      | 120K' 0
	add_dialog_item debian-live-$VERSION-source-mate+nonfree.log          ' * .log file                           |  10M' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.tar      'Standard desktop live source .tar file | 1.7G' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.contents ' * .contents file                      |  54K' 0
	add_dialog_item debian-live-$VERSION-source-standard+nonfree.log      ' * .log file                           | 3.7M' 0
	add_dialog_item debian-live-$VERSION-source-xfce+nonfree.tar          'Xfce desktop live source .tar file     | 5.0G' 0
	add_dialog_item debian-live-$VERSION-source-xfce+nonfree.contents     ' * .contents file                      | 124K' 0
	add_dialog_item debian-live-$VERSION-source-xfce+nonfree.log          ' * .log file                           |  10M' 0
	;;
debian-10.13.0-amd64-CD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2022-09-10 (Buster) AMD64 CDs"
	URL=$url_debian_10_13_0/$VERSION/amd64/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-netinst.iso     'Network installation CD         | 337M' 0
	add_dialog_item debian-$VERSION-amd64-xfce-CD-1.iso   'Xfce desktop installation CD #1 | 700M' 0
	add_dialog_item debian-edu-$VERSION-amd64-netinst.iso 'Edu network installation CD     | 407M' 0
	add_dialog_item debian-mac-$VERSION-amd64-netinst.iso 'Mac network installation CD     | 336M' 0
	;;
debian-10.13.0-amd64+nonfree-CD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2022-09-10 (Buster) AMD64 CDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION+nonfree/amd64/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-netinst.iso     'Network installation CD     | 380M' 0
	add_dialog_item firmware-edu-$VERSION-amd64-netinst.iso 'Edu network installation CD | 449M' 0
	;;
debian-10.13.0-i386-CD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2022-09-10 (Buster) i386 CDs"
	URL=$url_debian_10_13_0/$VERSION/i386/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-netinst.iso     'Network installation CD         | 429M' 0
	add_dialog_item debian-$VERSION-i386-xfce-CD-1.iso   'Xfce desktop installation CD #1 | 701M' 0
	add_dialog_item debian-edu-$VERSION-i386-netinst.iso 'Edu network installation CD     | 454M' 0
	add_dialog_item debian-mac-$VERSION-i386-netinst.iso 'Mac network installation CD     | 429M' 0
	;;
debian-10.13.0-i386+nonfree-CD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2022-09-10 (Buster) i386 CDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION+nonfree/i386/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-i386-netinst.iso     'Network installation CD     | 472M' 0
	add_dialog_item firmware-edu-$VERSION-i386-netinst.iso 'Edu network installation CD | 496M' 0
	;;
debian-10.13.0-amd64-DVD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2022-09-10 (Buster) AMD64 DVDs"
	URL=$url_debian_10_13_0/$VERSION/amd64/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-DVD-1.iso 'Installation DVD #1 | 3.6G' 0
	add_dialog_item debian-$VERSION-amd64-DVD-2.iso 'Installation DVD #2 | 4.4G' 0
	add_dialog_item debian-$VERSION-amd64-DVD-3.iso 'Installation DVD #3 | 4.4G' 0
	;;
debian-10.13.0-amd64+nonfree-DVD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2022-09-10 (Buster) AMD64 DVDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION+nonfree/amd64/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-DVD-1.iso 'Installation DVD #1 | 3.6G' 0
	;;
debian-10.13.0-i386-DVD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2022-09-10 (Buster) i386 DVDs"
	URL=$url_debian_10_13_0/$VERSION/i386/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-i386-DVD-1.iso 'Installation DVD #1 | 3.7G' 0
	add_dialog_item debian-$VERSION-i386-DVD-2.iso 'Installation DVD #2 | 4.4G' 0
	add_dialog_item debian-$VERSION-i386-DVD-3.iso 'Installation DVD #3 | 4.4G' 0
	;;
debian-10.13.0-i386+nonfree-DVD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2022-09-10 (Buster) i386 DVDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION+nonfree/i386/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-i386-DVD-1.iso 'Installation DVD #1 | 3.7G' 0
	;;
debian-10.13.0-amd64-i386-CD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2022-09-10 (Buster) AMD64+i386 CDs"
	URL=$url_debian_10_13_0/$VERSION/multi-arch/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-amd64-i386-netinst.iso 'Network installation CD | 618M' 0
	;;
debian-10.13.0-amd64-i386+nonfree-CD)
	VERSION=10.13.0
	DLTYPE="Unofficial Debian GNU/Linux $VERSION / 2022-09-10 (Buster) AMD64+i386 CDs (+non-free firmware)"
	URL=$url_debian_10_13_0_fw/$VERSION+nonfree/multi-arch/iso-cd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item firmware-$VERSION-amd64-i386-netinst.iso 'Network installation CD | 666M' 0
	;;
debian-10.13.0-source-DVD)
	VERSION=10.13.0
	DLTYPE="Official Debian GNU/Linux $VERSION / 2022-09-10 (Buster) source DVDs"
	URL=$url_debian_10_13_0/$VERSION/source/iso-dvd
	add_checksums_file MD5    MD5SUMS;    add_extra_file MD5SUMS.sign
	add_checksums_file SHA1   SHA1SUMS;   add_extra_file SHA1SUMS.sign
	add_checksums_file SHA256 SHA256SUMS; add_extra_file SHA256SUMS.sign
	add_checksums_file SHA512 SHA512SUMS; add_extra_file SHA512SUMS.sign
	add_dialog_item debian-$VERSION-source-DVD-1.iso  'Source DVD #1  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-2.iso  'Source DVD #2  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-3.iso  'Source DVD #3  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-4.iso  'Source DVD #4  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-5.iso  'Source DVD #5  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-6.iso  'Source DVD #6  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-7.iso  'Source DVD #7  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-8.iso  'Source DVD #8  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-9.iso  'Source DVD #9  | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-10.iso 'Source DVD #10 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-11.iso 'Source DVD #11 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-12.iso 'Source DVD #12 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-13.iso 'Source DVD #13 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-14.iso 'Source DVD #14 | 4.4G' 0
	add_dialog_item debian-$VERSION-source-DVD-15.iso 'Source DVD #15 | 3.5G' 0
	;;
