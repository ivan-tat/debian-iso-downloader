# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

devuan)
	DLTYPE="Devuan GNU+Linux Information and keys"
	URL=$url_devuan_release
	add_dialog_item README.txt                 'README.txt' 1
	add_dialog_item MIRRORS.txt                'MIRRORS.txt' 1
	add_dialog_item devuan-archive-keyring.gpg 'devuan-archive-keyring.gpg' 1
	add_dialog_item devuan-devs.gpg            'devuan-devs.gpg' 1
	;;
devuan-torrents)
	DLTYPE="Devuan GNU+Linux Torrent files"
	URL=$url_devuan_release
	add_dialog_item devuan_jessie.torrent   '1.x (Jessie) .torrent file' 0
	add_dialog_item devuan_ascii.torrent    '2.x (ASCII) .torrent file' 0
	add_dialog_item devuan_beowulf.torrent  '3.x (Beowulf) .torrent file' 0
	add_dialog_item devuan_chimaera.torrent '4.x (Chimaera) .torrent file' 0
	add_dialog_item devuan_daedalus.torrent '5.x (Daedalus) .torrent file' 0
	;;
