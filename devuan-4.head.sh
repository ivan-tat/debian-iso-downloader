# SPDX-FileType: SOURCE
# SPDX-FileCopyrightText: 2023 Ivan Tatarinov
# SPDX-License-Identifier: Unlicense

url_devuan_4=$url_devuan_release/devuan_chimaera

add_dialog_item devuan-4                   '4.x.x / 2021-10-14 (Chimaera) Information' 0
add_dialog_item devuan-4.0.3-desktop-live  '4.0.3 / 2023-04-30 (Chimaera) Desktop Live DVDs' 0
add_dialog_item devuan-4.0.3-minimal-live  '4.0.3 / 2023-04-30 (Chimaera) Minimal Live CDs' 0
add_dialog_item devuan-4.1.0-installer-iso '4.1.0 / 2022-02-15 (Chimaera) Installer CDs & DVDs' 0
